# Overview of changes

In approximate order of simplicity and importance (top to bottom)

Issue | code name                     | schema | info files | validated | coded | tested
----- | ----------------------------- | ------ | ---------- | --------- | ----- | ------
26    | `subnetwork`                  |  X     |     X      |   X       |   X   |   X
24    | `author_to_person`            |  X     |     X      |   X       |   X   |   X
21    | `rm_instrumentation_operator` |  X     |     NA     |   X       |   X    |  X
25    | `group_instrumentation`       |  X     |     X      |   X       |       |
19    | `operator_conform`            |  X     |     X      |   X     |   X  |  X
40    | `arol_compatibility`          |  X     |     X      |   X       |       |
45    | `require_stages`              |  X     |     X      |   X       |       |
23    | `uncertainty_objects`         |  X     |     X      |   X       |       |
36    | `base_configuration`          |  X     |     X      |   X       |       |
50    | `station_pids`                |  X     |     X      |           |       |
52    | `list_nomenclature`           |  X     |     X      |   X       |       |
14    | `fix_errors`                  |        |            |           |       |
51    | `multiple_equipments`         |        |            |           |       |
54    | `instrument_to_source`        |        |            |           |       |

# Details of changes

## `subnetwork`

**Replace `network` with `subnetwork`**

Avoids confusion with FDSN networks

## `author_to_person` 

**Make "person" (currently author) fields compatible with StationXML Person**

**v0.110**

``` yaml
  first_name: string
  last_name: string
  institution: string
  email: string
  phones: array of strings or [country_code, area_code, phone_number] objects
```

**0.111**

``` yaml
  name: string
  agencies: list of strings
  emails: list of strings
  phone: list of strings or [country_code, area_code, phone_number] objects
```
          
## `rm_instrumentation_operator`

**Remove intrumentation:operator field from schema**

Already does nothing but I kept it there to avoid breaking existing files

### Actions
Removed


## `group_instrumentation` 

**Bring instrumentation modifiers under `station:instrumentation`**

### Issue

And allow instrumentation-level configuration by adding ``modifications`` and ``config`` fields.

- This could also just be ``equipment_modifications`` as I think equipment is all that is modified
- `serial_number` is shorthand for (`serial_number` in ``equipment``), or maybe
  keep it as a shorthand for `equipment_modifications {serial_number: }`:
  declaring both should be considered an error (can it be done in
  the schema file?)

**v0.110**

```yaml
      instrumentation:
          {$ref: "xxx.instrumentation.yaml"}
      serial_number: string
      channel_modifications: object
```

**0.111**

``` yaml
  instrumentation:
      base: {$ref: "xxx.instrumentation.yaml"}
      serial_number: string
      config: string
      modifications: {equipment: object of equipment parameters to change}
      channel_modifications: object
```

or 

``` yaml
  instrumentation:
      base: {$ref: "xxx.instrumentation.yaml"}
      serial_number: string
      config: string
      equipment_modifications: object of equipment parameters to change
      channel_modifications: object
```

### Action

I implemented the second option

## `operator_conform`  

**Make "operator" conform to StationXML standard**

### Issue

Currently flat:

```yaml
    operator:
        reference_name:  # Just for link with campaign file, not StationXML
        full_name:   # Change to "agency"
        contact:    # Currently just a person's name
        phone_number:
        email:
        website: 
```

Should be

```yaml
    operator:
        reference_name:  # Just for link with campaign file, not StationXML
        agency:
        contact:
            name:
            agency:
            phone_number:
            email:
        website: 
```

The contact is a Person type and so can just be loaded from a "person" file

### Action

Implemented, but removed `reference_name` and placed it in new `subnetwork:reference_names`: object:

```yaml
	reference_names:
		campaign: "MYCAMPAIGN"
		operator: "INSU-IPGP"
```

(also replaces `subnetwork:campaign_ref_name`)

- Added "operator" to network-info (this replaces the `operator` at old `network` level).
- Renamed `stations_operator` to `operator` at `subnetwork` level
  for all stations inside
- Added `station:operator` field that can override subnetwork:operator
         
## `arol_compatibility`

**Increase compatibility with AROL**

Current differences with AROL (from readthedocs/advanced/arol) and what to do:

- change `offset` to `delay.samples`: I had thought that `offset` was better
  but it doesn't seem to mean what I thought it did.  AROL uses `delay.samples`
  in the FIR `filter` level: follow them
- change odd filter types back to non-camelcase: ADConversion->AD_CONVERSION,
  Analog->ANALOG, Digital->DIGITAL.  Our way was more consistent but we
  should be the same.  **LET'S SAY WE DO THIS BECAUSE THESE ARE NON-STATIONXML***
- rename "response_stages" to "stages" for closer compliance to AROL and
  StationXML
- allow complex numbers to be specified as "x+yj", where x and y are valid numbers

There are some other differences that don't seem like good ways for
`obsinfo` to go:

- AROL uses `units` instead of `transfer_function_type` in Pole/Zero filters.
  `transfer_function_type` is more directly compatible with StationXML
  (though less UCUM).  Equivalences are:
  AROL          |  obsinfo/StationXML
  ------------- | -------------------------
  “rad/s”       | “LAPLACE (RADIANS/SECOND)”
  “hz”          | “LAPLACE (HERTZ)”
  “z-transform” | “DIGITAL (Z-TRANSFORM)”

- AROL specifies both input_sample_rate and output_sample_rate for all stages.
- AROL specifies response stages thus:
  ``` yaml
      response:
          decimation_info:
              delay_correction: true
          stages:
  ```
  obsinfo simply specifies response_stages and the delay_correction attribute
  is specified at the datalogger level, as it is the only place where it
  makes sense for the global instrument.
  
      - Also, delay_correction is specified as either boolean in AROL or as
        a real number.
        In obsinfo a value of None is equivalent to AROL False and a numeric
        value is equivalent to AROL True.
        Proposed solution: make obsinfo read the AROL file and interpret
        this attribute.
        If found in a response other than the datalogger, give a warning and ignore.
        
### Actions
 
 - Used `delay.seconds` for PoleZero  and ANALOG filters, `delay.samples` for others
 - Made `ANALOG`, `DIGITAL` and `ADCONVERSION` uppercase.
 - for the complex_number type, I allow the old 2-tuple, or a string with pattern
   I found at https://regex101.com/library/6sKpkD , changing "i" to "j"
 - Renamed `response_stages` to `stages`
 - Put 'x _+ yj' in all pole-zero files
   
## `require_stages` 

**Make stages a required property (and channels not required)?**

The first is needed to create a proper channel.

The latter is not needed to create a proper station.

Can't implement the first before v0.111 because many example
InstrumentComponents did not have
response_stages in the "default" configuration.
This has been rectified, but people using these example components could
end up with broken code.

### Actions

- made `stages` required property of `datalogger`, `preamplifier` and `sensor`
- removed requirement for `channels` under `instrumentation_base` (not sure this is the
  best/final idea)
- **Also renamed `sensors/responses` directory to `sensors/stages`**
          
## `uncertainty_objects` 

**Make uncertainties into objects with measurement_method**  

Currently, azimuth.deg and dip.deg are 2-element arrays of number or null, with the first value being the value and the second the uncertainty, e.g.:
```python
azimuth.deg: [90, 5]
dip.deg: [0, 1]
```

Change them to be objects with the possible attributes specifed in the [StationXML Documetation](http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#azimuth):
```python
azimuth: {value.deg: 90, uncert.deg: 5, measurement_method: "Ppol"}
dip: {value.deg: 0, uncert.deg: 1, measurement_method: "Ppol"}
```
only `value.deg` should be required.

Should probably enable this for all fields that are defined as "FloatType" in the 1.1 XML schema:
- `WaterLevel` (units=m)
- `ClockDrift` (units=s?)
- `Response:Amplitude` (no units, specifed elsewhere?)
- `Response:Phase` (units=deg)
- `Response:Frequency` (units=Hz)
- `Decimation:InputSampleRate` (units=Hz)
- `PzTransferFunctionType:NormalizationFrequency` (units=Hz)
- `ApproximationType:FrequencyLowerBound` (units=Hz)
- `ApproximationType:FrequencyUpperBound` (units=Hz)
- `Decimation:Delay` (units=s)
- `Decimation:Correction` (units=s)
- `DipType` (units=deg)
- `Depth` (units=m)
- `SampleRate` (units=sps)

But allow value-only shortcut definitions, in which case `uncert` and `measurement_method` will be set to `null`, for exampe `WaterLevel.m: 0`,  `ClockDrift.s: 1e-10` and so on...
 
`Latitude.deg` `Longitude.deg` and `Elevation.m` are special cases for which the uncertainties and measurement method can be separately specified, as is currently the case.  

Or should I allow a separate _uncert variable for each field, in case they are the same for all instances (would only be inserted if the value was otherwise null)?

Perhaps have special `uncertainties` and `measurement_methods` objects that defined default uncertainties:

```python
uncertainties: {lat.m: 100, lon.m: 100, elevation.m: 10}
measurement_methods: {lat: "acoustic survey", lon: "acoustic survey", elevation: "acoustic survey", azimuth: "Ppol"}
```

for which I should probably keep the separation of uncertainties, measurement method and values

### Actions

**Only applied to azimuth and dip for now**

Created "definitions.schema.json#/float_type" element which is an object with
properties `value` (required), `uncertainty`, and `measurement_method`.
I don't think it's a good idea to also allow a simple float: forcing it to an object
makes it clear that other fields are possible.
Modified `azimuth.deg` and `dip.deg` elements to duplicate its operation but also
specify min and max values (couldn't figure out how to do using `floatType` and "allOf")
The `.deg` (or other) belongs in the field name and `value` and `uncertainty` should
to have the same units.

For positions, locations and their uncertainties are specified separately because
lats and lons positions are in degrees, whereas uncertainties are in meters.  This is
done under `station:locations:[LOCATION_CODE]:` as (in v0.110):

```yaml
base: 
	uncertainties.m: {lat: NUMBER, lon: NUMBER, elev: NUMBER}
	localization_method: STRING
	depth.m: NUMBER
	geology: STRING
	vault: STRING
position: {lat: NUMBER, lon: NUMBER, elev: NUMBER}
```
In v0.111 the `base` object renames `localization_method` to `measurement_method` and
adds the possibility to specify each parameter separately

```yaml
	measurement_method: STRING or {lat: STRING, lon: STRING, elev: STRING}
```

The `position` element is specified in `station.schema.json#/definitions/GeoJSON`
whereas the `base` field is specified in `location_base.schema.json`


     
## `base_configuration`

**Generalize base-configuration-modification**

schema files currently have no means to be configured (or modified?).

One solution would be to add to this level.

Another would to allow base, configuration and modification fields at any level:
if "base" is specified, then configuration and or modification can be put at
the same level.

### Actions

- Applied `base_config` interface at `instrumentation`, `instrument_component`,
  `stage`, `location` and `timing` levels (in schema and in example files).
- Wrote `information_files->base_config` documentation
- Combined `Scripps_SPOBS_HydroL22x{gain}_theoretical.stage.yaml` files into
  one file with different configurations (16x, 32x, 64x, 128x).
- Did same for Trillium Compact and 240 (and probably others)
- Now use "configuration" for both specification and modification (I started
using "config", both in files and in documentaion)**

**MAKE IT SO THAT IF THE CHANNEL default definition has a `configuration` and the
specific channel redefines that `instrument_component`, it does not inherit
the `configuration` (currently have to specify configuration=None)** (see
`BBOBS1_pre2012.instrumentation.yaml` and `BBOBS1_2012+.instrumentation.yaml`)

**Allow a default: ~ to force the user to provide a configuration? (for example,
to specify which BBOBS was deployed)**

**Allow only `configuration` to be specified in modifiers, keeps previous `base`**

**Note in documentation that locations also have a `base`, which is the same
idea (protection of modifications), but that a `position` is required outside
of `base`**

       
## `station_pids` 

**Allow putting PID in Stations**

AlpArray German stations (at GEOFON data center) have resourceIds for Datalogger, Sensor, PolesZeros, and FIR.
So, add resourceId to the Equipment and Filter classes

Actually, some German instruments use a custom <gfz:Identifier> field, which would require another type of obsinfo description such as:

```yaml
custom_field:
    namespace: (str, required)
    name: (str, required)
    attributes:
        whatever they want
    properties:
        whatever they want
```

or, simpler:

```yaml
custom_fields:
    - <gfz:Identifier type="hdl">10881/sensor.a7561d1a-d518-475d-9733-30370432996c</gfz:Identifier>
```

or, more specific:

```yaml
custom_identifier:
    namespace:
    type:
    value:
```

Should I also allow `resourceId` for Network, Station, Channel?

### Actions

- Added optional `resourceId` element to `subnetwork`, `station`, `channel`, Equipment and Filter elements
- Added optional `custom_fields` element to Equipment and Filter elements
              
## `list_nomenclature`   

**Standardize nomenclature for fields that can have more than one occurence?**

StationXML can have multiple occurences of one field (such as <Equipment>), but YAML/JSON cannot.  Should I:
 
1. specify each multiply-allowed field as an array (with "s" added to end of field name)?
2. allow single or multiple specifications ("s" for array, no "s" for singleton)?
3. Choose the most appropriate for each one?

I lean towards the first solution

Here are the fields I have found that allow multiple specifications, and how I have specified in obsinfo:

| StationXML field       | obsinfo v0.110               | obsinfo v0.111      | comment
| ---------------------- | ---------------------------- | ------------------- | ------------------
| `Network`              | network_info                 | *unchanged*         | should only have one network per subnetwork file
| `Operator`             | operator & stations_operator | **operators** and **stations_operators** | one operator per subnetwork file
| `Station`              | **stations**                 | *unchanged*
| `Equipment`            | equipment                    | **unchanged**       | otherwise configuring/changing gets too complicated (add possibility to add "equipments" at station level?)
| `ExternalReference`    | -                            | **external_references**
| `Channel`              | **instrumentation:channels** | *unchanged*
| `Channel:Type`         | -                            |
| `CalibrationDate`      | ?                            | **calibration_dates**
| `Author`               | **authors**                  | *unchanged*         |
| `Zero`                 | **zeros**                    | *unchanged*         |
| `Pole`                 | **poles**                    | *unchanged*         |
| `NumeratorCoefficient` | **numerator_coefficients**   | *unchanged*         |
| `Numerator`            | **numerator_coefficients**   | *unchanged*         |
| `Denominator`          | **denominator_coefficients** | *unchanged*         |
| `ResponseListElement`  | **elements**                 | *unchanged*         |
| `Coefficient`          | **coefficients**             | *unchanged*         |
| `Operator:Contact`     | contact                      | **contacts**        |
| `Person:Name`          | author:first_name/last_name  | **person:names**    |
| `Person:Agency`        | author:institution           | **person:agencies** |
| `Person:Email`         | author:email                 | **person:emails**   |
| `Person:Phone`         | author:phone_number          | **person:phones**   |
| `Stage`                | response_stages              | **stages**          |  Just renamed
| `BaseNode:Identifier`  | -                            | **identifiers**
| `BaseNode:Comment`     | **comments**                 | *unchanged*         |
| `DataAvailability:Span` | -                           |
| `namespace=="##other"` | -                            |
       
## `fix_errors`      

**StationXML errors to fix in new version 0.111**

The following errors, originally 6.3, 6.4, 8, 9, 10 and 12 in issue #3, will be addressed in v0.111.  Most require a new version, because we will need to add a `base` field to the `instrumentation` object in
order to bring station serial number and channel_modifications into this object:

1. No Serial Number shown for Station/Equipment
1. **LS5a is listed as 125 sps even though the network file says 62.5 sps**

And the following (possibly derived) errors:
1. Channel
   1. Sensor serial number is given as "32793N" (direct from instrumentation files, doesn't take into
      account OBS serial number)
1. Preamp/Datalogger/Equipment have no Serial Number

Moreover, I have to decide how to specify instrumentation-level serial numbers and configurations.  Should I imitate `channel_modifications` with and `instrumentation_modifications` (or just `modifications`?) field, or should I allow specific common fields such as `serial_number` and `instrumentation_configuration` (or `configuration`)? 

Other bugs that I didn't fix in v0.110.12:
1. Channel
   1. Sensor
      1. Has installation date, removal date and three calibration dates, all after expt
        (Trillium T240)
   1. PreAmplifier
      1. BBOBS gain card description is not specific enough (1x?  0.225x?)
1. No Comment (or field) saying how the station was located
1. Equipment description does not include configuration-specific information (need
   `configuration_description` field?)

**Should also make all tests work and maybe put my own tests back in**
           
### Actions

I think many of these errors were fixed in later revisions of v0.110.
I will wait until all the other changes are implemented before seeing what still needs
to be fixed

## `multiple_equipments`  

**Allow multiple Equipment(s) in station/instrument_components?**  

AlpArray German stations have multiple `<Equipment>`s for each station, corresponding to the frame and the two sensors
- StationXML allows multiple `Equipment` objects at Station and Channel levels
- The German implemenation goes against
  [FDSN rule](http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#equipment)
  that station Equipment should be common to all channels)
  
### Actions
Still deciding, but I may use `equipments` at appropriate level(s)
    
## `instrument_to_source`

**Change sensors' ` instrument` field to `source`?**    

To match new FDSN source identifiers specification

Should also allow `source` to be modified in preamplifier file (if not
already possible), as a bubblephone amplifier will change the codes from a hydrophone input.


## other

Other changes to make

tests
: datapath tests should be able to drill down in a directory and test all files
   within
   
 shortcuts
 : Add a shortcuts section to the documentation describing all shortcuts:
   - `preamplifier_config`, `datalogger_config`, `sensor_config`
   - specifying a gain w/o frequency (frequency defaults to 0)
   - `serial_number` for different elements

### Actions

make a time_base element and file with format
```
time_base:
	instrument: str
	reference: str
	start_sync_instrument: 0
```

# Summary of nomenclature changes


| v0.110                   | v0.111 | python file
| ------------------------ | -------------- | ------------------
| `network`                  | `subnetwork `           |
| `network:operator`         | `network_info:operator` |
| `network:station_operator` | `subnetwork:operator` |
| `operator:full_name`       | `agency ` |
| `operator:contact` (str)   | `contacts`  (`person` objects)|
| `operator:phone_number`    | `contacts:phones` |
| `operator:email`           | `contacts:emails` |
| `operator:reference_name`  | `*reference_names:operator`
| `campaign_ref_name`        | `*reference_names:campaign`
| `author:first/last_name`   | `person:name` | FDSN allows more than one
| `author:institution`       | `person:agencies`  |
| `author:email`             | `person:emails`    |
| `author:phone_number`      | `person:phones` |
| `response_stages`          | `stage`s         |
| `instrumentation`          | `instrumentation:base` |
| `station:serial_number`    | `instrumentation:serial_number`
| `station:channel_modifications` | `instrumentation:channel_modifications`
|                          | `instrumentation: configuration`
|                          | `instrumentation:equipment_modifications`
|                          | `instrumenation:preamplifier_configuration`
|                          | `instrumenation:sensor_configuration`
|                          | `instrumenation:datalogger_configuration`
| `filter:offset`            | `filter:delay_seconds` for `PoleZero`/`Analog`
|                          | `filter:delay_samles` for others
| `filter:Analog`	          | `filter:ANALOG`  | for AROL compatibility
| `filter:Digital`	          | `filter:DIGITAL` | for AROL compatibility
| `filter:ADConversion`	      | `filter:ADCONVERSION`  | for AROL compatibility
| `filter:PoleZeros:pole:(,)` | ...`'x + ij'`
| `azimuth.deg`: float        | `azimuth.deg`: floatType (`value:`, `*uncertainty:`, `*measurement_method:`)
| `dip.deg`: float           | `dip.deg`: floatType
| `location_base:localization_method`: str | `measurement_method`: str or {`lat`:str, `lon`:str, `elev`:str}
| `<instrument_component>`   | <instrument_component>:`base` | Carefully define/implement base-configuration behavior
|                          | <instrument_component>:`configuration`
|						  | <instrument_component>:<any_base_value>
| `stage`                    | `stage:base`
|                          | `stage:configuration`
|						  | `stage:<any_base_value>`
| `clock_correction_linear`  | `clock_correction_linear:base`
| `location_base`            | `location:base`
| -                        | `*subnetwork:resourceId`
| -                        | `*station:resourceId`
| -                        | `*channel:resourceId`
| -                        | `*equipment:resourceId`
| -                        | `*filter:resourceId`
| -                        | `*equipment:custom_fields`
| -                        | `*filter: custom_fields `
