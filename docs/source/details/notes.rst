****************
Notes
****************

Date formats
**************************

Dates should be entered in regular UTC format, either "yyyy-mm-dd" or 
"yyyy-mm-ddThh:mm:ssZ" according to norm 
`ISO 8601 <https://en.wikipedia.org/wiki/ISO_8601>`_. The "Z" specifies that
we are using UTC (GMT time). 

No effort is made to validate if dates are legal, i.e.,
to reject dates such as "31/02/2021".