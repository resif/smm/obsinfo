.. _JSON: https://www.json.org/json-en.html
.. _YAML: https://yaml.org
.. _ObsPy: https://github.com/obspy/obspy/wiki
.. _JSONref: https://datatracker.ietf.org/doc/html/draft-pbryan-zyp-json-ref-03
.. _YAML1.2: https://yaml.org/spec/1.2.2/
.. _XML: https://www.w3.org/XML/
.. _StationXML: http://docs.fdsn.org/projects/stationxml/en/latest/index.html

**************************
Comparison with StationXML
**************************

**obsinfo** information fields are as close to StationXML as feasible, but the
need to reduce duplicated information requires some changes.

Summmary of differences
=============================================

YAML_ instead of XML_
----------------------------------------------

Files are easier to read (as long as they are kept small)
and we can take advantage of the JSONref_ standard for importing
files

arrays instead of multiply-used fields:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

XML_ allows the same field name to be used repeatedly, YAML_ does not.
So multiply-used fields in StationXML_ are replaced with arrays,
with an "s" tacked onto the field name.  Some examples (noted as they
are in the following tables) are:

===================== ===================
StationXML field      obsinfo array
===================== ===================
Comment: (0+)         comments: []
CalibrationDate: (0+) calibration_dates: []
===================== ===================


Use of ``$ref`` s to insert other files
----------------------------------------------

Inherited from JSONref_ and is the key to reducing information to
atomic components

Use of `base` channels and specific modifiers
----------------------------------------------

Many channels have much the same information (position, start and enddates,
even sensors).  **obsinfo** therefore allows a `base` channel definition, which
is then modified by values placed in the specfic modifiers fields

Default inheritance of some fields
----------------------------------------------

In many cases, the `operator`, `location_code`, `start_date` and `end_date`
values are the same at the `subnetwork`, `station` and `channel` levels.
In StationXML they must be redefined in each level.
In **obsinfo**, if they are not defined at a level, they are inherited from the
level above.

No `sensitivity` stage in **obsinfo**
----------------------------------------------

The StationXML sensitivity stage is supposed to correspond to the sum of
the sensitivities of the underlying stages. **obsinfo** therefore simply calculates
this value from the provided stages

Handling of InstrumentComponents
----------------------------------------------
StationXML_ **Channel/Datalogger**, **Channel/Sensor** and
**Channel/Preamplifier** elements are actually **Equipment** objects.
These elements' response stages are mixed into the **Channel/Stages** list.

**obsinfo** ``datalogger``, ``sensor`` and (optional) ``preamplifier`` elements
contain ``equipment`` definitions, ``response_stage`` lists and elements
specific to each InstrumentComponent (``sensor`` : seed_code, azimuth, dip.
``datalogger``: sampling_rate) which are given flatly in the StationXML_
**Channel** level

The StationXML_ **InstrumentSensitivity** stage should equal
the sum of the sensitivities of the underlying stages at the given frequency.
**obsinfo** therefore does not ask for this value, but calculates it from the
provided stages

Handling of positions
----------------------------------------------
StationXML 1.2 specifies Station and Channel positions using the elements
**Latitude**, **Longitude** and **Elevation**, each of which is based
on the StationXML ``FloatType``` which includes optional **unit**, **plusError**,
**minusError** and **measurementMethod** elements

Latitudes and Longitudes are generally given in degrees and Elevation in meters,
but most most OBS (and land station) positions have an uncertainty that is
approximately constant (in meters), depending on the measurementMethod.

For this reason, **obsinfo** expresses positions and their uncertainties using
three elements:
- position: {lat.deg:, lon.deg:, elev.m:}
- uncertainties.m: {lat, lon, elev}
- measurement_method: string

This allows the position_uncertainty values to be associated with a measurement
method and entered separately from the actual instrument position.  **obsinfo**
then translates these values into StationXML FloatTypes with the appropriate units
arc-degrees, arc-degrees and meters), with the arc-degrees value depending on the 
station latitude.


Line-by-line comparison of differences
=============================================

Below is a line by line naming of StationXML 1.2 fields and their equivalent
name in **obsinfo** information files:

===========================   ==========================
StationXML *FDSNStationXML*   **obsinfo**
===========================   ==========================
Source                        *None*
Sender                        *None*
Module                        *None*
ModuleURI                     *None*
Created                       *Automatically calculated*
Network                       *See Network_ level*
===========================   ==========================

================================  =========================
StationXML *Network*              **obsinfo** ``network``
================================  =========================
code (*attribute*)                code
*startDate*  (*attribute*)        start_date:
*endDate* (*attribute*)           end_date:
*sourceID* *(attribute)*          source_id
*restrictedStatus* *(attribute)*  restricted_status
*Description*                     description
*Comment* (0+)                    comments
*Operator*                        operator
*Identifier*                      identifiers
*SelectedNumberStations*          *number of stations specified*
Station (0+)                      stations *see station_*
*DataAvailability*                *None*
*alternateCode* *(attribute)*     *None*
*historicalCode* *(attribute)*    *None*
================================  =========================

.. _station:

========================== ======================================== ===================
StationXML *Station*       **obsinfo** ``station``                      Notes
========================== ======================================== ===================
Operator:                  subnetwork:operator                      change in **obsinfo** in v0.111)
Description                description
Identifier                 identifiers
Latitude                   position: {lat.deg:, lon.deg:, elev.m:}
Longitude                  ""
Elevation                  ""
Site:                      site (only fills "Name" field)
WaterLevel                 water_level.m
Vault                      vault
Equipment:                 equipment (see equipment_)
ExternalReference: []      external_references
Channel: (0+)              channels: (see channels_)
DataAvailability           *None*
TerminationDate            *None*                                   (likely to be removed from StationXML_)
SelectedNumberChannels     *None*                                   (likely to be removed from StationXML_)
TotalNumberChannels        *None*                                   (likely to be removed from StationXML_)
========================== ======================================== ===================

.. _equipment:

=========================   ==========================
 StationXML *Equipment*     **obsinfo** ``equipment``      
=========================   ==========================
Description                 description         
Type                        type               
Manufacturer                manufacturer          
Model                       model                  
Vendor                      vendor                 
SerialNumber                serial_number          
InstallationDate            installation_date      
RemovalDate                 removal_date           
resourceId                  resource_id           
CalibrationDate: (0+)       calibration_dates: []  
=========================   ==========================

.. _channels:
.. _channel:

==================================== ================================ ===================
StationXML *Channel*                 **obsinfo** ``channel``              Notes
==================================== ================================ ===================
locationCode (attribute)
code *(attribute)*                    ``sensor/seed_codes``           also uses ``datalogger/sample_rate`` to decide band code
*startDate *(attribute)*
*endDate* *(attribute)*
*sourceID* *(attribute)*
*restrictedStatus* *(attribute)*
*alternateCode* *(attribute)*
*historicalCode* *(attribute)*
*Description:*          
*Identifier (0+)*          
*Comment (0+)*          
*DataAvailability*          
ExternalReference:          
Latitude          
Longitude          
Elevation          
Depth          
*Azimuth*                             ``sensor/azimuth``
Dip                                   ``sensor/dip``
Type:                                                                 (likely to be removed from StationXML)
SampleRate:                           ``datalogger/sample_rate``
SampleRateRatio:          
ClockDrift:           
*CalibrationUnits:*          
Sensor:                               ``sensor/equipment``
*PreAmplifier:*                       ``preamplifier/equipment``
DataLogger:                           ``preamplifier/equipment``      (in StationXML: not necessary at Channel level if set at Station level and same for all channels)
Equipment:                            ``equipment``
Response:
==================================== ================================ ===================

The StationXML_ **Response** level is replaced in **obsinfo** by the ``datalogger``
``preamplifier`` and ``sensor`` levels

========================== ================================ ===================
StationXML *Response*      **obsinfo** ``channel``              Notes
========================== ================================ ===================
resourceId (*attribute*)
InstrumentSensitivity      *calculated from Stages*
*InstrumentPolynomial*     *None*                           StationXML allows either InstrumentSensitivity or InstrumentPolynomial
Stage (0+)
                           datalogger
                           *preamplifier*
                           sensor
========================== ================================ ===================

========================== ================================ ===================
StationXML *Stage*         **obsinfo** ``stage``                Notes
========================== ================================ ===================
number (attribute)         *calculated by* **obsinfo**
resourceId (attribute)
{Type}                     filter/type:                     Can be PolesZeros, Coefficients, ResponseList, FIR or Polynomial
Decimation                                                  *DecimationType_*, Not in "Polynomials"
StageGain                                                   *GainType*: Value, Frequency. Not in "Polynomials"
========================== ================================ ===================

=========================== ================================ ===================
StationXML *DecimationType*         **obsinfo** ``stage``                Notes
=========================== ================================ ===================
InputSampleRate
Factor
Offset
Delay
Correction
=========================== ================================ ===================

========================== ================================ ===================
StationXML *PolesZeros*    **obsinfo** ``filter``               Notes
========================== ================================ ===================
                            type='PolesZeros'
name (attribute)
resourceId (*attribute*)
*Description*
InputUnits
OutputUnits
PzTransferFunctionType
NormalizationFactor
NormalizationFrequency
Zero: (0+)
Pole: (0+)
========================== ================================ ===================

========================== ================================ ===================
StationXML *Coefficients*  **obsinfo** ``filter``               Notes
========================== ================================ ===================
                            type='Coefficients'
name (attribute)
resourceId (*attribute*)
*Description*
InputUnits
OutputUnits
CfTransferFunctionType
Numerator: (0+)
Denominator: (0+)
========================== ================================ ===================

========================== ================================ ===================
StationXML *ResponseList*  **obsinfo** ``filter``                Notes
========================== ================================ ===================
                            type='ResponseList'
name (attribute)
resourceId (*attribute*)
*Description*
InputUnits
OutputUnits
ResponseListElement: (0+)                                   StationXML_: Frequency, Amplitude, Phase
========================== ================================ ===================

========================== ================================ ===================
StationXML *FIR*           **obsinfo** ``filter``                Notes
========================== ================================ ===================
                            type='FIR'
name (attribute)
resourceId (*attribute*)
*Description*
InputUnits
OutputUnits
Symmetry                                                    NONE, EVEN or ODD
NumeratorCoefficient: (0+)
========================== ================================ ===================

========================== ================================ ===================
StationXML *Polynomial*    **obsinfo** ``filter``                Notes
========================== ================================ ===================
                            type='Polynomial'               no **obsinfo** implementation
name (*attribute)*
resourceId (*attribute*)
*Description*
InputUnits
OutputUnits
ApproximationType                                           default="MACLAURIN">
FrequencyLowerBound
FrequencyUpperBound
ApproximationLowerBound
ApproximationUpperBound
MaximumError
Coefficient: (0+)
========================== ================================ ===================

.. _operator:

================================  =========================
StationXML *Operator*              **obsinfo** ``operator``
================================  =========================
Agency: string                    agency: string
Contact: []                       contacts: []
WebSite: URI                      website: uri
================================  =========================

.. _person:

================================  =========================
StationXML *PersonType*           **obsinfo** ``person``
================================  =========================
Name: (0+)                        name:
Agency: (0+)                      agencies: []
Email: (0+)                       emails: []
Phone: (0+)                       phones: [PhoneNumberType_]
================================  =========================

.. _PhoneNumberType:

================================  =========================
StationXML *PhoneNumberType*      **obsinfo**
================================  =========================
CountryCode
AreaCode
PhoneNumber
*name*
================================  =========================


================================  =========================
StationXML *uncertaintyDouble*      **obsinfo**
================================  =========================
                                  value:
*plusError* (attribute)           uncertainty: float
*minusError* (attribute)
*measurementMethod* (attribute)   measurement_method: string
================================  =========================


.. code-block:: yaml 

   FDSNStationXML:
     Network:
         Station:
             Channel:
               - *Description: string*
                 *Identifier:*
                   - string
                 *Comment:*
                   - Value: string
                     BeginEffectiveTime: '2017-08-25T05:02:50.47'
                     EndEffectiveTime: '2016-03-07T01:49:19.16'
                     Author: ''
                       - ''
                   - Value: string
                 *DataAvailability:*
                   Extent: ''
                   Span:
                     - ''
                 ExternalReference:
                   URI: <URI>
                   Description: string
                 Latitude: number
                 Longitude: number
                 Elevation: number
                 Depth: number
                 *Azimuth: number*
                 Dip: number
                 Type:
                   - [CONTINUOUS, HEALTH, SYNTHESIZED, GEOPHYSICAL, TRIGGERED, or WEATHER]
                 SampleRate: number
                 SampleRateRatio:
                   NumberSamples: number
                   NumberSeconds: number
                 ClockDrift: number
                 *CalibrationUnits:*
                   Name: string
                   Description: string
                 Sensor:
                   <equipment>
                 *PreAmplifier:*
                   <equipment>
                 DataLogger:
                   <equipment>
                 Equipment:
                   <equipment>
                 Response:
                   AnyElementYouLike: Some Data Or Other Elements
               - Identifier: string
       - ''
       - ''

.. code-block:: yaml
 
    <DecimationType
        InputSampleRate
        Factor
        Offset
        Delay
        Correction

.. code-block:: yaml 

    <BaseNodeType>
        code *(attribute)*
        *startDate *(attribute)*
        *endDate* *(attribute)*
        *sourceID* *(attribute)*
        *restrictedStatus* *(attribute)*
        *alternateCode* *(attribute)*
        *historicalCode* *(attribute)*
        *Description:*
        *Identifier (0+)*
        *Comment (0+)*
        *DataAvailability*

 .. code-block:: yaml 

    <BaseFilterType>
        name (attribute)
        resourceId (*attribute*)
        *Description*
        InputUnits
        OutputUnits

# stationxml 1.2 schema, with <annotations> and their contained <documentation>s removed

.. code-block:: xml

	<xs:element name="FDSNStationXML" type="fsx:RootType"/>
	<xs:complexType name="RootType">
		<xs:sequence>
			<xs:element name="Source" type="xs:string">
			<xs:element name="Sender" type="xs:string" minOccurs="0">
			<xs:element name="Module" type="xs:string" minOccurs="0">
			<xs:element name="ModuleURI" type="xs:anyURI" minOccurs="0">
			<xs:element name="Created" type="xs:dateTime">
			<xs:element name="Network" type="fsx:NetworkType" maxOccurs="unbounded"/>
			<xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
		<xs:attribute name="schemaVersion" type="xs:decimal" use="required">
		<xs:anyAttribute namespace="##other" processContents="lax"/>
	</xs:complexType>
	<xs:complexType name="NetworkType">
		<xs:complexContent>
			<xs:extension base="fsx:BaseNodeType">
				<xs:sequence>
					<xs:element name="Operator" type="fsx:OperatorType" minOccurs="0"
						maxOccurs="unbounded">
					<xs:element name="TotalNumberStations" type="fsx:CounterType" minOccurs="0">
					<xs:element name="SelectedNumberStations" type="fsx:CounterType" minOccurs="0">
					<xs:element name="Station" type="fsx:StationType" minOccurs="0"
						maxOccurs="unbounded"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
    <xs:complexType name="StationType">
        <xs:complexContent>
            <xs:extension base="fsx:BaseNodeType">
                <xs:sequence>
                    <xs:element name="Latitude" type="fsx:LatitudeType">
                    <xs:element name="Longitude" type="fsx:LongitudeType">
                    <xs:element name="Elevation" type="fsx:DistanceType">
                    <xs:element name="Site" type="fsx:SiteType">
                    <xs:element name="WaterLevel" type="fsx:FloatType" minOccurs="0">
                    <xs:element name="Vault" type="xs:string" minOccurs="0">
                    <xs:element name="Geology" type="xs:string" minOccurs="0">
                    <xs:element name="Equipment" type="fsx:EquipmentType" minOccurs="0"
                        maxOccurs="unbounded">
                    </xs:element>
                    <xs:element name="Operator" type="fsx:OperatorType" minOccurs="0"
                        maxOccurs="unbounded">
                    </xs:element>
                    <xs:element name="CreationDate" type="xs:dateTime" minOccurs="0">
                    <xs:element name="TerminationDate" type="xs:dateTime" minOccurs="0">
                    <xs:element name="TotalNumberChannels" type="fsx:CounterType" minOccurs="0">
                    <xs:element name="SelectedNumberChannels" type="fsx:CounterType" minOccurs="0">
                    <xs:element name="ExternalReference" type="fsx:ExternalReferenceType"
                        minOccurs="0" maxOccurs="unbounded">
                    <xs:element name="Channel" type="fsx:ChannelType" minOccurs="0"
                        maxOccurs="unbounded"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <!-- End StationType-->
    <xs:complexType name="ChannelType">
        <xs:complexContent>
            <xs:extension base="fsx:BaseNodeType">
                <xs:sequence>
                    <xs:element name="ExternalReference" type="fsx:ExternalReferenceType"
                        minOccurs="0" maxOccurs="unbounded">
                    </xs:element>
                    <xs:element name="Latitude" type="fsx:LatitudeType">
                    <xs:element name="Longitude" type="fsx:LongitudeType">
                    <xs:element name="Elevation" type="fsx:DistanceType">
                    <xs:element name="Depth" type="fsx:DistanceType">
                    <xs:element name="Azimuth" type="fsx:AzimuthType" minOccurs="0">
                    <xs:element name="Dip" type="fsx:DipType" minOccurs="0">
                    <xs:element name="WaterLevel" type="fsx:FloatType" minOccurs="0">
                    <xs:element name="Type" minOccurs="0" maxOccurs="unbounded">
                        <xs:simpleType>
                            <xs:restriction base="xs:NMTOKEN">
                                <xs:enumeration value="TRIGGERED"/>
                                <xs:enumeration value="CONTINUOUS"/>
                                <xs:enumeration value="HEALTH"/>
                                <xs:enumeration value="GEOPHYSICAL"/>
                                <xs:enumeration value="WEATHER"/>
                                <xs:enumeration value="FLAG"/>
                                <xs:enumeration value="SYNTHESIZED"/>
                                <xs:enumeration value="INPUT"/>
                                <xs:enumeration value="EXPERIMENTAL"/>
                                <xs:enumeration value="MAINTENANCE"/>
                                <xs:enumeration value="BEAM"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:group ref="fsx:SampleRateGroup" minOccurs="0"/>
                    <xs:element name="ClockDrift" minOccurs="0">
                        <xs:complexType>
                            <xs:simpleContent>
                                <xs:restriction base="fsx:FloatType">
                                    <xs:minInclusive value="0"/>
                                    <xs:attribute name="unit" type="xs:string" use="optional" fixed="SECONDS/SAMPLE">
                                        </xs:attribute>
                                </xs:restriction>
                            </xs:simpleContent>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="CalibrationUnits" type="fsx:UnitsType" minOccurs="0">
                    <xs:element name="Sensor" type="fsx:EquipmentType" minOccurs="0">
                    <xs:element name="PreAmplifier" type="fsx:EquipmentType" minOccurs="0">
                    <xs:element name="DataLogger" type="fsx:EquipmentType" minOccurs="0">
                    <xs:element name="Equipment" type="fsx:EquipmentType" minOccurs="0" maxOccurs="unbounded">
                    <xs:element name="Response" type="fsx:ResponseType" minOccurs="0"/>
                </xs:sequence>
                <xs:attribute name="locationCode" type="xs:string" use="required">
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <!-- End ChannelType -->
    <xs:complexType name="GainType">
        <xs:sequence>
            <xs:element name="Value" type="xs:double">
            <xs:element name="Frequency" type="xs:double">
        </xs:sequence>
    </xs:complexType>
    <xs:group name="FrequencyRangeGroup">
        <xs:sequence>
            <xs:element name="FrequencyStart" type="xs:double">
            <xs:element name="FrequencyEnd" type="xs:double">
            <xs:element name="FrequencyDBVariation" type="xs:double">
        </xs:sequence>
    </xs:group>
    <xs:complexType name="SensitivityType">
        <xs:complexContent>
            <xs:extension base="fsx:GainType">
                <xs:sequence>
                    <xs:element name="InputUnits" type="fsx:UnitsType">
                    <xs:element name="OutputUnits" type="fsx:UnitsType">
                    <xs:group ref="fsx:FrequencyRangeGroup" minOccurs="0">
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="EquipmentType">
        <xs:sequence>
            <xs:element name="Type" type="xs:string" minOccurs="0">
            <xs:element name="Description" type="xs:string" minOccurs="0">
            <xs:element name="Manufacturer" type="xs:string" minOccurs="0">
            <xs:element name="Vendor" type="xs:string" minOccurs="0">
            <xs:element name="Model" type="xs:string" minOccurs="0">
            <xs:element name="SerialNumber" type="xs:string" minOccurs="0">
            <xs:element name="InstallationDate" type="xs:dateTime" minOccurs="0">
            <xs:element name="RemovalDate" type="xs:dateTime" minOccurs="0">
            <xs:element name="CalibrationDate" type="xs:dateTime" minOccurs="0" maxOccurs="unbounded">
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="resourceId" type="xs:string" use="optional">
        </xs:attribute>
        <xs:anyAttribute namespace="##other" processContents="lax"/>
    </xs:complexType>
    <xs:complexType name="ResponseStageType">
        <xs:sequence>
            <xs:choice>
                <xs:sequence>
                    <xs:choice>
                        <xs:element name="PolesZeros" type="fsx:PolesZerosType" minOccurs="0">
                        <xs:element name="Coefficients" type="fsx:CoefficientsType" minOccurs="0"/>
                        <xs:element name="ResponseList" type="fsx:ResponseListType" minOccurs="0"/>
                        <xs:element name="FIR" type="fsx:FIRType" minOccurs="0">
                    </xs:choice>
                    <xs:element name="Decimation" type="fsx:DecimationType" minOccurs="0"/>
                    <xs:element name="StageGain" type="fsx:GainType">
                </xs:sequence>
                <xs:element name="Polynomial" type="fsx:PolynomialType">
            </xs:choice>
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="number" type="fsx:CounterType" use="required">
        <xs:attribute name="resourceId" type="xs:string">
        <xs:anyAttribute namespace="##other" processContents="lax"/>
    </xs:complexType>
    <xs:complexType name="CommentType">
        <xs:sequence>
            <xs:element name="Value" type="xs:string">
            <xs:element name="BeginEffectiveTime" type="xs:dateTime" minOccurs="0">
            <xs:element name="EndEffectiveTime" type="xs:dateTime" minOccurs="0">
            <xs:element name="Author" type="fsx:PersonType" minOccurs="0" maxOccurs="unbounded">
        </xs:sequence>
        <xs:attribute name="id" type="fsx:CounterType" use="optional">
        <xs:attribute name="subject" type="xs:string" use="optional">
    </xs:complexType>
    <xs:complexType name="PolesZerosType">
        <xs:complexContent>
            <xs:extension base="fsx:BaseFilterType">
                <xs:sequence>
                    <xs:element name="PzTransferFunctionType">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:enumeration value="LAPLACE (RADIANS/SECOND)"/>
                                <xs:enumeration value="LAPLACE (HERTZ)"/>
                                <xs:enumeration value="DIGITAL (Z-TRANSFORM)"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:element name="NormalizationFactor" type="xs:double" default="1.0">
                    <xs:element name="NormalizationFrequency" type="fsx:FrequencyType">
                    <xs:element name="Zero" type="fsx:PoleZeroType" minOccurs="0" maxOccurs="unbounded">
                    <xs:element name="Pole" type="fsx:PoleZeroType" minOccurs="0" maxOccurs="unbounded">
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="FIRType">
        <xs:complexContent>
            <xs:extension base="fsx:BaseFilterType">
                <xs:sequence>
                    <xs:element name="Symmetry">
                        <xs:simpleType>
                            <xs:restriction base="xs:NMTOKEN">
                                <xs:enumeration value="NONE"/>
                                <xs:enumeration value="EVEN"/>
                                <xs:enumeration value="ODD"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:element name="NumeratorCoefficient" minOccurs="0" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:simpleContent>
                                <xs:extension base="xs:double">
                                    <xs:attribute name="i" type="xs:integer"/>
                                </xs:extension>
                            </xs:simpleContent>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="CoefficientsType">
        <xs:complexContent>
            <xs:extension base="fsx:BaseFilterType">
                <xs:sequence>
                    <xs:element name="CfTransferFunctionType">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:enumeration value="ANALOG (RADIANS/SECOND)"/>
                                <xs:enumeration value="ANALOG (HERTZ)"/>
                                <xs:enumeration value="DIGITAL"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:element name="Numerator" minOccurs="0" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:simpleContent>
                                <xs:extension base="fsx:FloatNoUnitType">
                                    <xs:attribute name="number" type="fsx:CounterType"/>
                                </xs:extension>
                            </xs:simpleContent>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="Denominator" minOccurs="0" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:simpleContent>
                                <xs:extension base="fsx:FloatNoUnitType">
                                    <xs:attribute name="number" type="fsx:CounterType"/>
                                </xs:extension>
                            </xs:simpleContent>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="ResponseListElementType">
        <xs:sequence>
            <xs:element name="Frequency" type="fsx:FrequencyType"/>
            <xs:element name="Amplitude" type="fsx:FloatType"/>
            <xs:element name="Phase" type="fsx:AngleType"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="ResponseListType">
        <xs:complexContent>
            <xs:extension base="fsx:BaseFilterType">
                <xs:sequence>
                    <xs:element name="ResponseListElement" type="fsx:ResponseListElementType"
                        minOccurs="0" maxOccurs="unbounded"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="PolynomialType">
        <xs:complexContent>
            <xs:extension base="fsx:BaseFilterType">
                <xs:sequence>
                    <xs:element name="ApproximationType" default="MACLAURIN">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:enumeration value="MACLAURIN"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:element name="FrequencyLowerBound" type="fsx:FrequencyType">
                    <xs:element name="FrequencyUpperBound" type="fsx:FrequencyType">
                    <xs:element name="ApproximationLowerBound" type="xs:double">
                    <xs:element name="ApproximationUpperBound" type="xs:double">
                    <xs:element name="MaximumError" type="xs:double">
                    <xs:element name="Coefficient" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:simpleContent>
                                <xs:extension base="fsx:FloatNoUnitType">
                                    <xs:attribute name="number" type="fsx:CounterType"/>
                                </xs:extension>
                            </xs:simpleContent>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    <xs:complexType name="DecimationType">
        <xs:sequence>
            <xs:element name="InputSampleRate" type="fsx:FrequencyType"/>
            <xs:element name="Factor" type="xs:integer">
            <xs:element name="Offset" type="xs:integer">
            <xs:element name="Delay" type="fsx:FloatType">
            <xs:element name="Correction" type="fsx:FloatType">
        </xs:sequence>
    </xs:complexType>
    <!-- The following elements represent numbers. -->
    <xs:attributeGroup name="uncertaintyDouble">
        <xs:attribute name="plusError" type="xs:double" use="optional">
        <xs:attribute name="minusError" type="xs:double" use="optional">
        <xs:attribute name="measurementMethod" type="xs:string" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="FloatNoUnitType">
        <xs:simpleContent>
            <xs:extension base="xs:double">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:extension>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="FloatType">
        <xs:simpleContent>
            <xs:extension base="xs:double">
                <xs:attribute name="unit" type="xs:string" use="optional">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:extension>
        </xs:simpleContent>
    </xs:complexType>
    <!-- Derived from FloatType. -->
    <xs:complexType name="SecondType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:attribute name="unit" type="xs:string" fixed="SECONDS">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="VoltageType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:attribute name="unit" type="xs:string" fixed="VOLTS">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="AngleType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:minInclusive value="-360"/>
                <xs:maxInclusive value="360"/>
                <xs:attribute name="unit" type="xs:string" use="optional" fixed="DEGREES">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="LatitudeBaseType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:minInclusive value="-90"/>
                <xs:maxExclusive value="90"/>
                <xs:attribute name="unit" type="xs:string" use="optional" fixed="DEGREES">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="LatitudeType">
        <xs:simpleContent>
            <xs:extension base="fsx:LatitudeBaseType">
                <xs:attribute name="datum" type="xs:NMTOKEN" use="optional" default="WGS84"/>
            </xs:extension>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="LongitudeBaseType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:minInclusive value="-180"/>
                <xs:maxInclusive value="180"/>
                <xs:attribute name="unit" type="xs:string" use="optional" fixed="DEGREES">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="LongitudeType">
        <xs:simpleContent>
            <xs:extension base="fsx:LongitudeBaseType">
                <xs:attribute name="datum" type="xs:NMTOKEN" use="optional" default="WGS84"/>
            </xs:extension>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="AzimuthType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:minInclusive value="0"/>
                <xs:maxExclusive value="360"/>
                <xs:attribute name="unit" type="xs:string" use="optional" fixed="DEGREES">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="DipType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:minInclusive value="-90"/>
                <xs:maxInclusive value="90"/>
                <xs:attribute name="unit" type="xs:string" use="optional" fixed="DEGREES">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="DistanceType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:attribute name="unit" type="xs:string" use="optional" default="METERS">
                <xs:attributeGroup ref="fsx:uncertaintyDouble"/>
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="FrequencyType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:attribute name="unit" type="xs:string" use="optional" fixed="HERTZ">
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:group name="SampleRateGroup">
        <xs:sequence>
            <xs:element name="SampleRate" type="fsx:SampleRateType">
            <xs:element name="SampleRateRatio" type="fsx:SampleRateRatioType" minOccurs="0">
        </xs:sequence>
    </xs:group>
    <xs:complexType name="SampleRateType">
        <xs:simpleContent>
            <xs:restriction base="fsx:FloatType">
                <xs:attribute name="unit" type="xs:string" use="optional" fixed="SAMPLES/S">
            </xs:restriction>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="SampleRateRatioType">
        <xs:sequence>
            <xs:element name="NumberSamples" type="xs:integer">
            <xs:element name="NumberSeconds" type="xs:integer">
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="PoleZeroType">
        <xs:sequence>
            <xs:element name="Real" type="fsx:FloatNoUnitType">
            <xs:element name="Imaginary" type="fsx:FloatNoUnitType">
        </xs:sequence>
        <xs:attribute name="number" type="xs:integer">
    </xs:complexType>
    <xs:simpleType name="CounterType">
        <xs:restriction base="xs:integer">
            <xs:minInclusive value="0"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="OperatorType">
        <xs:sequence>
            <xs:element name="Agency" type="xs:string">
            <xs:element name="Contact" type="fsx:PersonType" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="WebSite" type="xs:anyURI" minOccurs="0">
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="PersonType">
        <xs:sequence>
            <xs:element name="Name" type="xs:string" minOccurs="0" maxOccurs="unbounded">
            <xs:element name="Agency" type="xs:string" minOccurs="0" maxOccurs="unbounded">
            <xs:element name="Email" type="fsx:EmailType" minOccurs="0" maxOccurs="unbounded">
            <xs:element name="Phone" type="fsx:PhoneNumberType" minOccurs="0" maxOccurs="unbounded">
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="SiteType">
        <xs:sequence>
            <xs:element name="Name" type="xs:string">
            <xs:element name="Description" type="xs:string" minOccurs="0">
            <xs:element name="Town" type="xs:string" minOccurs="0">
            <xs:element name="County" type="xs:string" minOccurs="0">
            <xs:element name="Region" type="xs:string" minOccurs="0">
            <xs:element name="Country" type="xs:string" minOccurs="0">
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:anyAttribute namespace="##other" processContents="lax"/>
    </xs:complexType>
    <xs:complexType name="ExternalReferenceType">
        <xs:sequence>
            <xs:element name="URI" type="xs:anyURI">
            <xs:element name="Description" type="xs:string">
        </xs:sequence>
    </xs:complexType>
    <!-- Simple types -->
    <xs:simpleType name="NominalType">
        <xs:restriction base="xs:NMTOKEN">
            <xs:enumeration value="NOMINAL"/>
            <xs:enumeration value="CALCULATED"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="EmailType">
        <xs:restriction base="xs:string">
            <xs:pattern value="[\w\.\-_]+@[\w\.\-_]+"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="PhoneNumberType">
        <xs:sequence>
            <xs:element name="CountryCode" type="xs:integer" minOccurs="0">
            <xs:element name="AreaCode" type="xs:integer">
            <xs:element name="PhoneNumber">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:pattern value="[0-9]+-[0-9]+"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="description" type="xs:string" use="optional"/>
    </xs:complexType>
    <xs:simpleType name="RestrictedStatusType">
        <xs:restriction base="xs:NMTOKEN">
            <xs:enumeration value="open"/>
            <xs:enumeration value="closed"/>
            <xs:enumeration value="partial"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="UnitsType">
        <xs:sequence>
            <xs:element name="Name" type="xs:string">
            <xs:element name="Description" type="xs:string" minOccurs="0">
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="IdentifierType">
        <xs:simpleContent>
            <xs:extension base="xs:string">
                <xs:attribute name="type" type="xs:string">
                </xs:attribute>
            </xs:extension>
        </xs:simpleContent>
    </xs:complexType>
    <xs:complexType name="BaseFilterType">
        <xs:sequence>
            <xs:element name="Description" type="xs:string" minOccurs="0">
            <xs:element name="InputUnits" type="fsx:UnitsType">
            <xs:element name="OutputUnits" type="fsx:UnitsType">
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="resourceId" type="xs:string">
        <xs:attribute name="name" type="xs:string">
        <xs:anyAttribute namespace="##other" processContents="lax"/>
    </xs:complexType>
    <xs:complexType name="ResponseType">
        <xs:sequence>
            <xs:choice minOccurs="0">
                <xs:element name="InstrumentSensitivity" type="fsx:SensitivityType" minOccurs="0">
                <xs:element name="InstrumentPolynomial" type="fsx:PolynomialType" minOccurs="0">
            </xs:choice>
            <xs:element name="Stage" type="fsx:ResponseStageType" minOccurs="0"
                maxOccurs="unbounded">
            </xs:element>
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="resourceId" type="xs:string">
        <xs:anyAttribute namespace="##other" processContents="lax"/>
    </xs:complexType>
    <xs:complexType name="DataAvailabilityExtentType">
        <xs:attribute name="start" type="xs:dateTime" use="required">
        <xs:attribute name="end" type="xs:dateTime" use="required">
        <xs:anyAttribute namespace="##other" processContents="lax"/>
    </xs:complexType>
    <xs:complexType name="DataAvailabilitySpanType">
        <xs:attribute name="start" type="xs:dateTime" use="required">
        <xs:attribute name="end" type="xs:dateTime" use="required">
        <xs:attribute name="numberSegments" type="xs:integer" use="required">
        <xs:attribute name="maximumTimeTear" type="xs:decimal" use="optional">
        <xs:anyAttribute namespace="##other" processContents="lax"/>
    </xs:complexType>
    <xs:complexType name="DataAvailabilityType">
        <xs:sequence>
            <xs:element name="Extent" type="fsx:DataAvailabilityExtentType" minOccurs="0"/>
            <xs:element name="Span" type="fsx:DataAvailabilitySpanType" minOccurs="0" maxOccurs="unbounded"/>
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:anyAttribute namespace="##other" processContents="lax"/>
    </xs:complexType>

