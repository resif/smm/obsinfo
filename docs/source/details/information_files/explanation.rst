.. _information_files_explanation:

*****************
Explanation
*****************

Here are examples of information files, from the most basic to the most
complete.  You can also see the schemas at :ref:`Schemas`

.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
  
  explanation/1_overview
  explanation/2_base_config
  explanation/3_compare_stationxml
  explanation/4_compare_arol
  explanation/examples_subnetwork
