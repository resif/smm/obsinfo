.. _templates:

*****************
Templates
*****************

Here are template files for each type of information file. 
Every possible element is included, with optional parameters marked.
For details on formatting, see the schemas at :ref:`Schemas`

Subnetwork and friends
**************************

.. _subnetwork_template:

subnetwork
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.subnetwork.yaml
    :language: yaml

.. _location_base_template:

location_base
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.location_base.yaml
    :language: yaml

.. _network_template:

network
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.network.yaml
    :language: yaml

.. _operator_template:

operator
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.operator.yaml
    :language: yaml

.. _person_template:

person
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.person.yaml
    :language: yaml

.. _timing_base_template:

timing_base
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.timing_base.yaml
    :language: yaml

Instrumentation
**************************

.. _instrumentation_base_template:

instrumentation_base
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.instrumentation_base.yaml
    :language: yaml

Instrument_components and stages
*********************************

.. _datalogger_base_template:

datalogger_base
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.datalogger_base.yaml
    :language: yaml

.. _preamplifier_base_template:

preamplifier_base
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.preamplifier_base.yaml
    :language: yaml

.. _sensor_base_template:

sensor_base
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.sensor_base.yaml
    :language: yaml

.. _stage_base_template:

stage_base
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.stage_base.yaml
    :language: yaml

.. _Filter_templates:

Filters
*********************************

.. _ad_conversion_filter_template:

ADCONVERSION
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.AD_Conversion.filter.yaml
    :language: yaml

.. _analog_filter_template:

ANALOG
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.analog.filter.yaml
    :language: yaml

.. _coefficients_filter_template:

Coefficients
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.coefficients.filter.yaml
    :language: yaml

.. _digital_filter_template:

DIGITAL
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.digital.filter.yaml
    :language: yaml

.. _fir_filter_template:

FIR
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.FIR.filter.yaml
    :language: yaml

.. _poles_zeros_filter_template:

PolesZeros
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.poleszeros.filter.yaml
    :language: yaml

.. _polynomial_filter_template:

Polynomial
===============================================

Note: Polynomial filters don't work yet in **obsinfo**, because they don't exist
in **obspy**!

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.polynomial.filter.yaml
    :language: yaml

.. _response_list_filter_template:

ResponseList
===============================================

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.response_list.filter.yaml
    :language: yaml

