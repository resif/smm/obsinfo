****************
Best practices
****************

Place instrumentation information files in a central repository
*******************************************************************************

One of the main pillars of *obsinfo* philosophy is reuse and the DRY (don't
repeat yourself) principle.
In order to achieve this every effort has been made to ensure reusability,
but the ultimate responsibiity for this lies with the user.
It is strongly recommended to create a central repository of instrumentation
information files which can then be reused by several campaigns.
Instrumentations should be flexible enough, with several typical configurations,
so the need to make modifications through ``channel_modifications`` is minimized.

The use of a central repository will also permit information to be protected
assigning modification writes only to the responsible parties.

Campaign, subnetwork and station files can then be placed in different directories
according to users and teams.

Use a file hierarchy for different objects
***********************************************************

Although *obsinfo* gives you total flexibility to organize your files as you see fit, 
it is recommended to use a hierarchy of directories and the ``obsinfo_datapath`` variable setup 
with the ``obsinfo-setup`` application, whose used is explained in the :ref:`InstallStartup`. 

Validate information files bottom up
***********************************************************

Before trying to create a Station XML file, all information files should be
validated individually.
The best way to do this is bottom up: first validate filters, then
stages, then components, then instrumentations, then subnetworks.
This minimizes large output messages which are difficult to parse. 

Files in central repositories should never be uploaded unless they have been
validated.


Reuse information files
*****************************************

Either create a repository of your own or use one of the central repository.
If you plan on working offline, you can clone the GitLab repository locally.

Document information files with notes, extras and comments
**********************************************************************

A choice of documentation options are available in information files.
Aside of the "#" comment mechanism of the YAML language, ``notes`` are used
for *obsinfo* documentation that will **not** be seen in the resulting
StationXML file. On the other hand, ``comments`` at the
``network``, ``station`` and ``channel`` levels will be incorporated into
the StationXML file. 
``extras`` can be specified at these levels as well.
These take the form of key/value pairs to simulate attributes which are not
actually present in StationXML but can be used for documentation.
A typical example is:

.. code-block:: yaml

   DBIRD_response_type : "CALIBRATED" 
  
which is used in several filters.
It should be specified at the channel level, perhaps specifying to
which filters it applies.

Placement of ``stages`` in information file
************************************************************************

Although ``stages`` can be specified before the ``configuration_definitions``,
this is discouraged unless there is a single configuration.
If ``stages`` is specified outide of the ``configuration_definitions``, it is
considered the default ``stages``.

- if the chosen configuration defines ``stages``, the ``default stages``
  is thrown away
- if the chosen configuration does not define ``stages``, the ``default stages``
  is used
- if no configuration is specified, the ``default stages`` is used

In the following example, "responses/INSU_BBOBS_gain0.225_theoretical.stage.yaml"
will *always* be overwritten by "responses/INSU_SPOBS_L28x128_theoretical.stage.yaml":

.. code-block:: yaml

 preamplifier:
     equipment:
         model: "GEOPHONE-GAIN"
         type: "Analog gain/filter card"
         description: "SIO gain/filter card, seismo channel"
         manufacturer: "SIO or IPGP"
         vendor: ""
     configuration_default: "128x gain"
     stages: 
         - $ref: "responses/INSU_BBOBS_gain0.225_theoretical.stage.yaml" # ALWAYS OVERWRITTEN
     configuration_definitions:
         "128x gain":
             stages:
                - $ref: "responses/INSU_SPOBS_L28x128_theoretical.stage.yaml#stage"

The following example is perfectly OK, as there is a single
configuration and no need to specify ``configuration_definitions``: 

.. code-block:: yaml

 preamplifier:
     equipment:
         model: "GEOPHONE-GAIN"
         type: "Analog gain/filter card"
         description: "SIO gain/filter card, seismo channel"
         manufacturer: "SIO or IPGP"
         vendor: ""
     stages:
         - $ref: "responses/INSU_BBOBS_gain0.225_theoretical.stage.yaml#"


How to modify individual stages
*************************************************

Stage modification must always be specified in
``stage_modifications`` at the instrument component level.
This will NOT work:

.. code-block:: yaml  

     preamplifier:  
        {$ref: "preamplifiers/LCHEAPO_BBOBS.preamplifier.yaml#preamplifier"} 
        gain: 
           value: 34

This will:

.. code-block:: yaml

     preamplifier:  
        {$ref: "preamplifiers/LCHEAPO_BBOBS.preamplifier.yaml#preamplifier"} 
        stage_modifications:                                              
           "*": gain: 
                  value: 34

Use of channel modifications
*************************************************

A large and flexible instrumentation database, with lots of possible
configurations, will rarely need ``channel_modifications``.
If they must be used, remember to **always** use the complete hierarchical syntax.
No shortcuts are allowed.

For example, to change the gain of a sensor stage you need to write:

.. code-block:: yaml
 
  channel_modifications:
     sensor:  
         {$ref: "sensors/SIO_DPG.sensor.yaml#sensor"}
         stage_modifications:                                              
           "*": gain: 
                  value: 34

and not a shortcut such as:

.. code-block:: yaml
 
  channel_modifications:
     gain: 
       value: 34

as there is no way *obsinfo* can determine to which stage of which component
you want to apply the gain modification.


Base your info files on templates
*************************************************

While syntax may be a challenge, we recommend strongly that, when starting a
new information file, you use a template.
That way at least you can guarantee that the indentation and spelling of
attributes is right.
