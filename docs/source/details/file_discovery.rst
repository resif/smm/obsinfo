.. _file_discovery:

==================
File discovery
==================

``obsinfo setup`` sets several variables specific to *obsinfo* and stores them
in a configuration file in your home directory called ``.obsinforc``.
Here are the variables, with their default values:

.. code-block:: console

 gitlab_repository: www.gitlab.com
 gitlab_project: resif/obsinfo
 gitlab_path: _examples/instrumentation_files
 datapath:  [https://www.gitlab.com/resif/obsinfo/obsinfo/_examples/instrumentation_files]
 obsinfo_branch: master

``gitlab_repository``, ``gitlab_project`` and ``gitlab_path`` indicate the location
of a remote repository for information files. 
``datapath`` works like the ``PATH`` variable in Linux: directories
specified (remote or local) are searched **INE THE ORDER THEY ARE LISTED**.
By default, only the remote repository is listed, but if you use the ``-D`` option
the directory your created will be listed first.  You might want to add `.` to the
beginning of the list so that obsinfo looks first in the directory in which you
are working.

Finally, since the remote repository is assumed to be a gitlab repository,
we must specify a gitlab branch, which by  default is ``master``.

There are many optional parameters, which you can see using ` obsinfo setup`-h``:

See :ref:`obsinfo setup` for the help output

**IMPORTANT**: In ``datapath``, the remote reoository address is always the
concatenation of ``gitlab_repository``,   ``gitlab_project`` and ``gitlab_path``.
So if you manually change the remote repository address in the ``datapath``
line of the ``.obsinforc`` file you will not get the expected result (always use
``obsinfo-setup``).

**(The following is moved from tutorial 1)**

Finding the information files is one of the most important features in *obsinfo*.
Notice that the  pathnames in the examples above are not absolute (i.e. they
don't start at the base of the filesystem). 
In regular POSIX practice it is assumed that non-absolute paths are added to
the directory where the application is executed  (called the current working directory, or cwd).
To allow the user to store instrumentation files in one standard spot, *obsinfo*
will try to *discover* the file in one of several directories specified by the
variable ``obsinfo_datapath``, which is set by the ``obsinfo setup`` application 
and found in the configuration file ``~/.obsinforc`` (``~`` is  your home directory). 
This works much in the same way that executables are found in Linux, MacOS
or Windows using the variable PATH.

Whenever a file in a ``$ref`` is specified without an absolute path, 
*obsinfo* will **sequentally** look for the file in all the directories specified
in ``obsinfo_datapath``.
A special keyword, ``GITLAB``, specifies a remote Gitlab repository. Here's an example:

``obsinfo_datapath``, as specified above, will always look in a local directory where the 
current examples are installed (via ``pip`` or ``conda``) and then, if not found, in the 
remote repository. This gives the user great flexibility, as (s)he can override an existing 
remote information file with a local one, change the order of discovery, etc.

In the end, you will create your own information files in a directory selected by you.
Then you  will have to edit the ``obsinfo_datapath`` variable to reflect the directory
from which you want to access your information files.

It is possible, although not recommended, to use absolute paths.

Use of slashes (/) instead of Windows backslashes (\) is recommended for
uniformity, so a file can be used on different operation systems.
However, if you use backslashes, *obsinfo* will understand them.
