****************
Caveats
****************

Caveat: Effect of ``$ref``
*****************************

``$ref`` totally substitutes that line by the content referenced in the file
path. This is equivalent to writing the entire content of referenced file
instead of the ``$ref`` line.
This should be taken into account for syntax purposes.
If syntax is not validated, there is a good chance is that the ``$ref`` file
syntax is either wrong or in the wrong place.
Not finding the file also causes problems which are not evident at first glance;
if you keep getting errors, check if the file is in the right place. 

Caveat: Cryptic syntax messages
**********************************

Unfortunately, the JSON/YAML parser is very terse reporting syntax errors
such as unmatched parentheses, brackets or curly brackets, single items instead
of lists, etc., usually result in an exception with a cryptic message.
It is *strongly* recommended that you edit YAML files in a suitable editor
which can check at least basic syntax errors.
See also the Troubleshooting section below. 

Caveat: Stage numbering
***********************************

Although stages are listed for each instrumebt_component (sensor,
preamplifier and datalogger), they are combined into a single list
for the whole instrument, in the order: sensor->preamplifier->datalogger.
Within a component, they are taken in the order specified in the information file.
They are numbered from one to *n*, starting with the first ``sensor`` stage
and ending with the last ``datalogger`` stage. 

If you modify stages within an instrument component, the stage numbers are relevant
to that instrument_component.
If you modify stages within an instrumentation, the stage numbers are relative to the stacked
sensor+preamplifer+datalogger.
For example, if you want to modify the first stage within a datalogger and you are within
the datalogger specification, you would refer to it as stage 1.  If you want to
modify that same stage at the instrument level, you would refer to it as stage
nS+nP+1, where nS is the number of sensor stages and nP is the number of preamplifier
stages.

Caveat: Treatment of sample rates in response stages
***********************************************************
Only the first datalogger's stage's ``input_sample_rate``` should be specified
(usually the ADConversion stage). All other input and output sample rates are
calculated using the (**REQUIRED!**) ``decimation_factor`` of each digital stage.

The output sample rate is calculated for the whole response and checked against
the sample rate specified in the datalogger.
A warning will be issued if they are different.

Caveat: ALWAYS follow the syntax and beware of $ref overwriting your attributes
********************************************************************************

A naïve approach to syntax might think that we can add fields at the same
level as ``$ref`` specification.
For example, we could be tempted to add a datalogger configuration to 
a specified file, like so:

..block-code:: yaml

 instrumentation:
     base:
        "$ref" : "instrumentations/BBOBS1_2012+.instrumentation.yaml" 
        datalogger_config: "62.5sps"

But "$ref" will substitute all attributes at the same level, erasing the
attribute ``datalogger_config``.
If this happens it will be a silent error, since substitution occurs before
validation and the application will never know ``datalogger_config`` was there.
The correct way of applying ``datalogger_config`` is at the same level as ``base``
