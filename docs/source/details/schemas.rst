.. _Schemas:

***********************************************
Schema files
***********************************************

Below are the JSON schemas for every file type, plus the ``definitions`` schema file
that is used by several other schema files

===================================
Corresponding to a file type
===================================

dataloggers_base files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/datalogger_base.schema.json
    :language: json

.. _Filter_files:

filter files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/filter.schema.json
    :language: json

instrumentation_base files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/instrumentation_base.schema.json
    :language: json

location_base files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/location_base.schema.json
    :language: json

network files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/network.schema.json
    :language: json

operator files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/operator.schema.json
    :language: json

person files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/person.schema.json
    :language: json

preamplifier_base files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/preamplifier_base.schema.json
    :language: json

sensor_base files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/sensor_base.schema.json
    :language: json

stage_base files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/stage_base.schema.json
    :language: json

subnetwork files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/subnetwork.schema.json
    :language: json

timing_base files
===============================================

.. literalinclude:: ../../../src/obsinfo/data/schemas/timing_base.schema.json
    :language: json

===================================
Others
===================================

These either are used by many file types or simplify the definition of a file type

stages
===============================================

Used by ``datalogger_base``, ``sensor_base`` and ``preamplifier_base``

.. literalinclude:: ../../../src/obsinfo/data/schemas/stages.schema.json
    :language: json

station
===============================================

Used by ``subnetwork``

.. literalinclude:: ../../../src/obsinfo/data/schemas/station.schema.json
    :language: json

definitions
===============================================

Used by several file types

.. literalinclude:: ../../../src/obsinfo/data/schemas/definitions.schema.json
    :language: json

iris_units
===============================================

Only used by stage_base schema, but could be useful for others...

.. literalinclude:: ../../../src/obsinfo/data/schemas/definitions.schema.json
    :language: json


