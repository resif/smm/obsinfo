*******************************
Nomenclature
*******************************

Terms that have specific meanings in the **obsinfo** universe

instrumentation
    A complete instrumentation (such as an OBS)
    
instrument
    One channel of an instrumentation, minus the orientation information
    
instrument_component
    One component of an instrument: a datalogger, sensor, or pre-amplifier

element
  Anything in the information file written as a key in a ``key: value`` pair

key
    Must be a string
  
