.. _advanced_base_configuration_modifications:

********************************
Base-configuration-modifications
********************************

.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
  
  base_config_modif/explanation
  base_config_modif/abstract_examples
  base_config_modif/concrete_examples
