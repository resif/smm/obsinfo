.. _training_course_4:

**********************************************
Advanced issues
**********************************************


Creating a processing pathway
=====================================================

The directory ``src/obsinfo/addons/`` contains programs to create
processing scripts using the information in the network files.
These scripts depend on the external programs used: ``obsinfo`` currently has
programs for:

+------------------------------------------------------------+--------------+---------------------------------+
| Transformation                                             |  file        |  command line                   |
+============================================================+==============+=================================+
| LCHEAPO OBS data to miniSEED using the SDPCHAIN tools      | LCHEAPO.py   | ``obsinfo scripts_LCHEAPO``     |
+------------------------------------------------------------+--------------+---------------------------------+
| uncorrected to corrected miniSEED using the SDPCHAIN tools | SDPCHAIN.py  | ``obsinfo scripts_SDPCHAIN``    |
+------------------------------------------------------------+--------------+---------------------------------+
| LCHEAPO OBS data to (poorly) corrected SDS structure       | LS2SDS.py    | ``obsinfo scripts_LC2SDS``      |
+------------------------------------------------------------+--------------+---------------------------------+

These probably aren't directly applicable to other OBS facilities, but the files
can serve as a basis for your own codes.
The command line programs are created using the ``console_scripts`` parameter
in ``obsinfo/setup.py``


Storing and accessing your instrument database online
=====================================================

I have never done this,
Luis Arean discusses it in :ref:`file_discovery`
