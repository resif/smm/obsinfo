.. _training_course_1:

*****************************
Introduction
*****************************

----------------------------
Structure and Philosophy
----------------------------

obsinfo_ is a system to create standard seismological metadata
files (currently StationXML), as well as processing flows specific to ocean
bottom seismometer (OBS) data.  It's basic philosophy is:

1) Put the complete metadata information in text files (no dependence
   on command-line arguments or interactive entry)
#) Allow complete specification of a data collection campaign in a single
   file (with links to instrumentation files)
#) Break down every component of the system into "atomic", non-repetitive units.
#) Follow StationXML structure where possible, but:

    a) Add entities missing from StationXML where necessary
    #) Use appropriate units for each component, for example:

       - Specify the digital filter ``delay`` as ``delay.samples``
       - Specify lat and lon in degrees but their uncertainties in meters
         (obsinfo_ does the conversion to degrees for the StationXML file)

An overview video and presentation can be found on the
`Orfeus workshops page <http://www.orfeus-eu.org/other/workshops/>`_, under 
"ORFEUS Webinars:Introduction to obsinfo"

Basic structure
===================================================

**obsinfo** files are arranged in a hierarchy, from :ref:`Subnetwork_Files`
down to :ref:`Filter_files` files.
The hierarchy is shown in the :ref:`object_model` and explained
in the :ref:`Tutorial on General Structure <Tutorial2>`


File formats
===================================================

obsinfo_ uses :ref:`information files <information_files_overview>` in JSON_
or YAML_ format.
YAML_ is generally easier to write and read by humans, whereas JSON_ is easier for computers.
The tutorial includes a section describing YAML_ files as used in obsinfo_
(:ref:`Tutorial1`).
There are many sites for converting from one format to the other and for
validating either format: including this json-to-yaml-convertor_ and this
yaml-validator_.

.. _YAML: https://yaml.org
.. _JSON: https://www.json.org
.. _JSONRef: http://jsonref.org
.. _json-to-yaml-convertor: https://onlineyamltools.com/convert-json-to-yaml
.. _yaml-validator: https://codebeautify.org/yaml-validator


Tutorial
===================================================

This training course is meant to accompany an instructor.
The :doc:`../tutorial` provides a more detailed step-by-step explanation and we
refer to :doc:`../tutorial` sections throughout this training course.

Command-line programs
===================================================

_obsinfo provides several command-line programs.
They all start with **obsinfo-**, so in many shells you should be able
to see them by typing "obsinfo-<TAB>"

- ``obsinfo xml`` makes stationXML files from an obsinfo subnetwork file and its dependencies
- ``obsinfo validate`` validates subnetwork, instrumentation, datalogger, sensor, preamplifier, stage and filter files
- ``obsinfo print`` prints information about obsinfo_ files, by ingesting the files
  into obsinfo_ classes and then printing out the class string representations.
- ``obsinfo setup`` creates the .obsinforc file and can also create an example database.
- ``obsinfo version`` prints the current version code.

There are also a few ``obsinfo scripts-*`` command-line programs,
which are used for making IPGP-specific data processing flows.
They could be used as a basis for creating your own data processing flows.

----------------------------
Compared to other systems
----------------------------

File format
===================================================

Compared to StationXML files
-------------------------------------------

- Minimizes repeated information

    - for example, in StationXML 
    
      - Each channel could have the same datalogger but all of the
        datalogger specifications are repeated for each channel.
      - Within a channel's response itself, several of the stages may be identical
        (except for the ``offset``).  

- Eliminate fields that can be calculated from other fields, such as:

    - The ``<InstrumentSensitivity>`` field, which depends on the ``Stage`` s
      that follow
    - The ``<Delay>`` for digital filter stages, which can be calculated from
      ``delay.samples`` / ``input_sample_rate``

Compared to RESP files
-------------------------------------------
RESP files (mostly used in the Nominal Reference Library) are text
representations of the Dataless SEED files that preceded the StationXML
standard, so they share the repetitive nature of StationXML files and add
the complexity of a non-standard text format.

Compared to AROL
-------------------------------------------
The Atomic Response Objects Library (AROL_)
replaces the RESP-based Nominal Response Library in the new YASMINE system.
Files use the same atomic concept and YAML structure as ``obsinfo``, in fact
the AROL format was based on a previous version of obsinfo and we try to keep
the two compatible.

AROL lacks the ``subnetwork``, ``station`` and ``instrumentation`` levels as
these are assembled by YASMINE.

.. _AROL: https://gitlab.com/resif/arol

Metadata creation
===================================================

**obsinfo** uses the command-line program ``obsinfo-makeStationXML`` on a
:ref:`subnetwork <subnetwork_examples>` file to create StationXML metadata files.
All options are stored in the :ref:`subnetwork <subnetwork_examples>` file and
the instrumentation files it references.

Compared to PDCC
-------------------------------------------
PDCC is a graphical user interface allowing one to assemble different components
(sensors, dataloggers, amplifiers) and add in deployment information.
Components can be added from the Nominal Response Library (NRL_),
which combines RESP files with textual configuration files which allow the
user to select the exact component and configuration they used.

**obsinfo** uses a fully textual description of instruments and deployments
rather than a graphical user interface.

.. _NRL: http://ds.iris.edu/ds/nrl/

Compared to IRIS DMC IRISWS_
-------------------------------------------
I don't know much about this, it looks like a webservice to obtain component responses but I'm not sure how you're supposed
to assemble them.  It might just be a more modern way to access the NRL components that is supposed to be used by newer systems. 

.. _IRISWS: https://service.iris.edu/irisws/nrl/1/

Compared to YASMINE
-------------------------------------------
YASMINE is a new StationXML metadata creation tool.
It's major difference from PDCC is its use of atomic response files,
which should be compatible with obsinfo files.
It provides a graphical user interface (YASMINE-EDITOR_) and a command-line 
interface (YASMINE-CLI_).
The major differences from ``obsinfo`` are the lack of ``instrumentation``,
``station`` and ``subnetwork`` levels, a more individual-station based processing
protocal, and lack of OBS-specific processing information such
as instrument clock drift 

.. _YASMINE-EDITOR: https://github.com/iris-edu/yasmine-stationxml-editor
.. _YASMINE-CLI: https://github.com/iris-edu/yasmine-stationxml-cli


----------------------------
Datafile Processing
----------------------------

The directory ``obsinfo/obsinfo/addons/`` contains programs to create
processing scripts using the information in the subnetwork files.

This is addressed in more detail in the :doc:`2d_advanced` module

.. _obsinfo: https://gitlab.com/resif/obsinfo