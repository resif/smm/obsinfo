.. _training_course_1b_subnetwork:

*************************
Subnetwork files
*************************

:ref:`subnetwork <subnetwork_examples>` files are top-level files in **obsinfo**.
They contain information specific to a set of instrument deployments and are
generally all that needs to be filled in for a given deployment.
Here is a link to the :ref:`subnetwork tutorial <Tutorial3>`
Below is a typical subnetwork file: 

.. code-block:: yaml

    format_version: "1.0"
    revision:
        date: "2022-10-01"
        authors:
            - {$ref: persons/Jackson_Brown.person.yaml#person}
            - {$ref: persons/Old_Macdonald.person.yaml#person}
    subnetwork:
        operators:
            - {$ref: operators/DEPAS.operator.yaml#operator}
        network: {$ref: networks/AlpArray.network.yaml#network}
        stations:
            "STA1":
                site: "East Edge"
                start_date: "2022-01-01T12:00:01Z"
                end_date: "2022-11-01T13:00:01Z"
                locations:
                    "00":
                        base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
                        configuration: "BUC_DROP"
                        position: {lon.deg: -32.234, lat.deg: 37.2806, elev.m: -1950}
                location_code: "00"
                instrumentation:
                    base: {$ref: instrumentations/NAMMU.instrumentation_base.yaml#instrumentation_base}
                    serial_number: "A2030"
                    datalogger_configuration: "200sps"
                processing:
                    - clock_correction_linear:
                            base: {$ref: "timing_bases/Seascan_GNSS.timing_base.yaml#timing_base"}
                            start_sync_reference: "2022-01-01T11:20:00Z"
                            end_sync_reference: "2022-11-01T14:00:00.2450Z"
                            end_sync_instrument: "2022-11-01T14:00:00Z"       
            "STA1":
                site: "West Edge"
                start_date: "2022-01-01T13:00:01Z"
                end_date: "2022-11-01T14:00:01Z"
                locations:
                    "00":
                        base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
                        configuration: "SURFACE_DEPLOY"
                        position: {lon.deg: -33.234, lat.deg: 38.2806, elev.m: -2950}
                instrumentation:
                    base: {$ref: instrumentations/NAMMU.instrumentation_base.yaml#instrumentation_base}
                    serial_number: "A0012"
                    datalogger_configuration: "200sps"
                processing:
                    - clock_correction_linear:
                            base: {$ref: "timing_bases/Seascan_GNSS.timing_base.yaml#timing_base"}
                            start_sync_reference: "2022-01-01T11:20:00"
                            end_sync_reference: "2022-11-01T14:00:00.2450"
                            end_sync_instrument: "2022-11-01T14:00:00"       
           <STATIONNAME3>:
                ...
            ...

The ``format_version`` and ``revision``` elements give information about the file itself, while the third,
``subnetwork``, provides information about the deployments themselves.
To repeat as little information as possible, commonly repeated elements such
as the network, the instrumentation, the operators and even some highly specific
information about the timing base and location uncertainties are placed in other
files that are referred to here using the ``$ref`` element.
Any of these elements could actually be filled in directly in the subelement file,
but this leads to repetition of information and should be avoided.

Below is a list of the full range of ``subnetwork`` elements 
(starred fields are optional):

.. code-block:: yaml

    subnetwork:
        network: <$ref to NETWORK file>
        operators: [] # list of <$ref to OPERATOR file>
        stations:
            <STATIONNAME1>:
                site: <string>
                start_date: <string>
                end_date: <string>
                locations: {}
                location_code: <string>
                instrumentation:
                    base: <$ref to INSTRUMENTATION_BASE file>
                    *serial_number: <string>
                    *modifications: {}
                    *channel_modifications: {}
                *notes: []  # list of strings
                *comments: [] # list of strings or {value: , begin_effective_time: , end_effective_time: , authors: }
                *operators: [] # list of <$ref to OPERATOR file>
                *source_id: <string> # URI format
                *external_references: <list of {uri: , description: }> 
                *identifiers: <list of strings> # URI format
                *water_level.m: 
                *description: <string>
                *restricted_status: <string>  # "open", "closed", or "partial"
                *processing:
                    - *clock_correction_linear: {}
                    - *clock_correction_leapsecond: {}
                *extras: {}
        *comments: []*
        *extras: {}*
        *reference_names: {operator: , campaign: }
            <STATIONNAME2>:
                ...


The referred files are explained later in this Training Course


Comments,  notes and extras
===================================================

Comments and notes are both lists of text.

``comments`` can be entered at the ``subnetwork``, ``station`` and ``channel`` level and
will be transformed into StationXML comments at the same level.
Individual ``comments`` can be either pure text or dicts with elements correponding
to the `StationXML Comment`_ element

.. _StationXML Comment: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=comment#comment

``notes`` will not go into the StationXML file, they are for your information
only.  
They can be entered at the ``base``, ``station``, and ``component`` levels.

``extras`` is a free object-based field.
It can be used to add fields that may be useful in a future
version of obsinfo.
Nothing there is put into the StationXML code unless the obsinfo software
is specifically updated to do so ( which allows new fields without breaking
compatibilty or schema rules).
``extras`` can be entered at  the ``subnetwork``, ``station`` or ``channel`` level

Details
===================================================

- Referenced files referenced are searched for starting at the paths given in
  the ``~/.obsinforc`` file
