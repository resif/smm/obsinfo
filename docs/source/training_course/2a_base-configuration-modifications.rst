.. _training_course_3_base-configuration-modifications:

***********************************************
The base-configuration-modifications structure
***********************************************

In the example subnetwork_ files, you probably saw that some elements
have a ``base`` subelement.
This is actually part of a ``base-configuration-modifications`` structure that
allows you to efficiently specify configurations and changes to instrumentations.
For a detailed description of this structure,
see :ref:`advanced_base_configuration_modifications`,

``modifications`` safely update the referred element in the ``base``.
For example:

.. code-block:: yaml

    instrumentation:
        base: {$ref: instrumentation/MY_INSTRUMENT.instrumenation_base#instrumentation_base}
        modifications:
            equipment: {serial_number: "B0034"}

will update ``base:equipment:serial_number`` to "B0034" without modifying any
other ``base`` or ``base:equipment`` values.

``configurations`` are simply ``modifications`` specified in the base element,
for example:

.. code-block:: yaml

    instrumentation_base:
        equipment:
            ...
        channels:
            ...
        configurations:
            "SN01":
                equipment: {serial_number: '01'}
                channels:
                    default: {sensor: {configuration: "Sphere01"}}
                    "4": {sensor: {configuration: "LDEO4702"}}
            "SN02":
                equipment: {serial_number: '02'}
                channels:
                    default: {sensor: {configuration: "Sphere02"}}
                    "4": {sensor: {configuration: "SIO8165"}} 

In the above example, each configuration sets the instrumentation serial
number and configures the default sensors attached to each instrumentation.

``modifications`` are applied after ``configuration``, so if the
same element is specified in both, the ``modifications`` value will used.

obsinfo_ elements using base-configuration-modification are

- ``instrumentation``
- ``datalogger``, ``sensor`` and ``preamplier``
- ``stage``
- ``location``
- ``clock_correction_linear``

The basic structure
===================================================

.. code-block:: yaml

    instrumentation:
        base: {$ref: instrumentation/MY_INSTRUMENT.instrumenation_base#instrumentation_base}
        configuration: "SN02"
        modifications:
            equipment: {serial_number: "B0034"}
        
The ``configuration`` element selects the "SN02" configuration.
The ``modifications`` element updates the equipment serial number. 


``channel_modifications`` and ``stage_modifications``
=====================================================

``channel_modifications`` and ``stage_modifications`` allow you to change
element/s on a specified channel or stage.
``channel_modifications`` only exist for ``instrumentation``, whereas
``stage_modifications`` exist for ``instrumentation:channel_modifications``,
``datalogger``, ``sensor``, and ``preamplifier``.

The code below:

- Sets the datalogger configuration to '200sps' on all channels.
- Replaces the sensor on the "H" (hydrophone) channel (the "replace_" before "sensor"
  means to replace, not update, the sensor).
- Changes the "Z" channel's stage 1 gain to 4000.

.. code-block:: yaml

    instrumentation:
        base: {$ref: instrumentation/MY_INSTRUMENT.instrumenation_base#instrumentation_base}
        configuration: "NINJA"
        modifications:
            equipment: {serial_number: "B0034"}
        channel_modifications:
            '*-*': {datalogger: {configuration: '200sps'}}
            'H-*':
                replace_sensor:
                    base: {$ref: sensors/MY_SENSOR.sensor_base#sensor_base}
                    modifications: {equipment: {serial_number: "HT105"}}
            'Z-*':
                stage_modifications:
                    '1': {gain: 4000}

``channel_modifications`` and ``stage_modifications`` use text **keys** to
indicate which channel/stage to update.

- For ``channel_modifications``:

    - the key text before the "-" is the channel code (or '*')
    - the key text after the "-" is the location code (or '*')
    
- For ``stage_modifications``:

    - the key text can be a specfic stage number or a
      regex code, such as "*", "[1, 3, 4]" or "[1-3]".

Modifications under more specific (without '*') keys update/replace
modifications under less-specific keys.
The full language is specified in :ref:`advanced_channel_modifications`
        
Shortcuts
===================================================

The ``serial_number`` and ``datalogger_configuration`` shortcuts allow you
to specify these commonly-used modifications more compactly.
Using these shortcuts, the previous code block becomes:

.. code-block:: yaml

    instrumentation:
        base: {$ref: instrumentation/MY_INSTRUMENT.instrumenation_base#instrumentation_base}
        configuration: "NINJA"
        serial_number: "B0034"
        datalogger_configuration: '200sps'
        channel_modifications:
            'H-*':
                replace_sensor:
                    base: {$ref: sensors/MY_SENSOR.sensor_base#sensor_base}
                    serial_number: "HT105"
            'Z-*':
                stage_modifications:
                    '1': {gain: 4000}


Non-base elements
===================================================

The ``location`` and ``clock_correction_linear`` elements have some sub-elements
that are different for each station and so do not belong in the ``base``.
These elements are:
 - location: ``position``
 - clock_correction_linear: ``start_sync_reference``, ``end_sync_reference``,
   ``end_sync_instrument``
 
They are therefore entered at the same level as ``base``, ``configuration``,
and ``modifications``:

example using location
-------------------------------------

.. code-block:: yaml

    locations:
        "00":
            base: {location_bases/MY_BASE.location_base.yaml#location_base}
            configuration: "SEA_SURFACE"
            position: {lat.deg: , lon.deg: , elev.m: } # lat and lon in degrees, elev in meters

The "SEA_SURFACE" configuration specifies typical location uncertainties for
a location corresponding to the sea surface deployment point:

.. code-block:: yaml

    location_base:
        depth.m: 0
        geology: "unknown"
        vault: "Sea floor"
        uncertainties.m: {lon: 200, lat: 200, elev: 20}
        measurement_method: "Sea surface release point"
        configuration_default: "SEA_SURFACE"
        configurations:
            "SEA_SURFACE":
                configuration_description: "Standard sea-surface deployment"
            "ACOUSTIC_SURVEY":
                uncertainties.m: {lon: 5, lat: 5, elev: 10}
                measurement_method: "Acoustic survey"
            "AIRGUN_SURVEY":
                uncertainties.m: {lon: 40, lat: 40, elev: 40}
                measurement_method: "Airgun survey"
                notes: ["Uncertainty will generally be least along-line"]

example using clock_correction_linear
-------------------------------------

.. code-block:: yaml

    clock_correction_linear:
        base: {timing_bases/MY_BASE.timing_base.yaml#timing_base}
        start_sync_reference: "2020-01-01T12:00:00Z"
        end_sync_reference: "2021-02-22T08:00:00.456Z"
        end_sync_instrument: "2021-02-22T08:00:00Z"
    
Here the instrument clock drifted by 0.456 seconds over the length
of the experiment.
Since we specify absolute times rather than a "skew" or "drift" value,
there is no ambiguity over the polarity of the "skew" or "drift" value.
Note that ``start_sync_instrument`` is not specified.
This means that it is assumed to be the same as ``start_sync_reference``
If it is not the same, you can also specify ``start_sync_instrument``.


Other sources
===================================================

- Channel modifications are described briefly in :doc:`/tutorial/3_subnetwork`
- Component configurations are described in :doc:`/tutorial/4_instrumentation`
  and :doc:`/tutorial/5_instrument_component`
  and :doc:`/tutorial/6_datalogger`

.. _obsinfo: https://gitlab.com/resif/obsinfo