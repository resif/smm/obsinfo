************************************************
Day 2: Instrumentation files and advanced topics
************************************************

.. toctree::
  :caption:  Table of contents
  :maxdepth: 1

  2a_base-configuration-modifications
  2b_instrumentation
  2c_specific_issues
  2d_advanced
  