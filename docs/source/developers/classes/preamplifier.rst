.. _Preamplifier11:

****************
Preamplifier
****************

===============
 Description
===============

An optional preamplifier may be part of an OBS instrument.
It is an :ref:`InstrumentComponent <InstrumentComponent>` with response stages
and no particular attributes of its own.

---------------------
Python class:
---------------------

Preamplifier

---------------------
 YAML / JSON label:
---------------------

preamplifier

------------------------------------------
Corresponding StationXML structure
------------------------------------------

Preamplifier

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

:ref:`InstrumentComponent <InstrumentComponent>`

-----------------------------------------
Subclasses
-----------------------------------------

* :ref:`Filter <Filter11>`

-----------------------------------------
Relationships
-----------------------------------------

* Is element of :ref:`Equipment <Equipment>`
* Contains :ref:`Response Stages <Stages>`

==============================
Attributes
==============================

*None*

*For the rest of attributes, see superclass :ref:`InstrumentComponent <InstrumentComponent>`*


==============================
JSON schema
==============================

`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/preamplifier_base.schema.json>`_

==============================
Example
==============================

file: `_templates/TEMPLATE.preamplifier_base.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.preamplifier_base.yaml
  :language: YAML

==================
Class Navigation
==================

:ref:`InstrumentComponent` \<\=\=\> :ref:`Stages`

           

