.. _Sensor:

***************
Sensor
***************

===============
 Description
===============

A sensor is an :ref:`InstrumentComponent` belonging to an :ref:`Instrument11`. It models an OBS sensor and so is the generator of the signal being processed. Inheriting from InstrumentComponent, it has all its attributes plus the ones below.

---------------------
Python class:
---------------------

Sensor

---------------------
 YAML / JSON label:
---------------------

sensor
   Sensor usually has its own information file (best practice)

------------------------------------------
Corresponding StationXML structure
------------------------------------------

Channel.Sensor

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

:ref:`InstrumentComponent`

-----------------------------------------
Subclasses
-----------------------------------------

*None*

-----------------------------------------
Relationships
-----------------------------------------

* Contains :ref:`Stages`
* Is part of an :ref:`Instrument11`


==============================
Attributes
==============================

.. _code: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#channel

======================== ================================== ============ ================= =============================== =======================================================================
        **Name**                   **Type**                 **Required**    **Default**    **Equivalent StationXML**            **Remarks**
------------------------ ---------------------------------- ------------ ----------------- ------------------------------- -----------------------------------------------------------------------
  seed_codes                      :ref:`SeedCodes11`                Y           *None*            Channel. `code`_                See explanation in class :ref:`SeedCodes11`
                                                                                                                                  Only first two codes set here. Orientation set at channel level.
======================== ================================== ============ ================= =============================== =======================================================================

*For the rest of attributes, see superclass :ref:`InstrumentComponent <InstrumentComponent>`*

==============================
JSON schema
==============================

`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/sensor_base.schema.json>`_

==============================
Example
==============================

file: `_templates/TEMPLATE.sensor_base.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.sensor_base.yaml
  :language: YAML

==================
Class Navigation
==================


:ref:`InstrumentComponent` \<\=\=\> :ref:`Stages`
