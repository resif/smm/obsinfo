
.. _Polynomial11:

***************
Polynomial
***************


===============
 Description
===============

A :ref:`filter <Filter11>` response  given in terms of a polynomial expansion
of powers of the sensor output signal.
The coefficients are Maclaurin coefficients for the given
frequency and approximation ranges.


---------------------
Python class:
---------------------

Polynomial

---------------------
 YAML / JSON label:
---------------------

polynomial

------------------------------------------
Corresponding StationXML structure
------------------------------------------

Polynomial

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

:ref:`Filter_Template <Filter11>`

-----------------------------------------
Subclasses
-----------------------------------------

*None*

-----------------------------------------
Relationships
-----------------------------------------

* Is nested in :ref:`Stage <Stage>`

==============================
Attributes
==============================

========================= ================================== ============ ================= ========================== =========================================
        **Name**                    **Type**                 **Required**    **Default**    **Equivalent StationXML**                   **Remarks**
------------------------- ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
type                         string  (must be "Polynomial")        Y               None          None
------------------------- ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
coefficients                   Array of Maclaurin 
                               Coefficients                      Y           *None*         `Coefficients`_   
------------------------- ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
frequency_lower_bound        number                              Y           *None*         `FrequencyLowerBound`_
------------------------- ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
frequency_upper_bound        number                              Y           *None*         `FrequencyUpperBound`_
------------------------- ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
approximation_lower_bound    number                              Y           *None*         `ApproximationLowerBound`_
------------------------- ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
approximation_upper_bound    number                              Y           *None*         `ApproximationUpperBound`_
------------------------- ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
maximum_error                number                              Y           *None*         `MaximumError`_
------------------------- ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
approximation_type           string (must be "MACLAURIN")        Y           *None*         `ApproximationType`_
------------------------- ---------------------------------- ------------ ----------------- -------------------------- -----------------------------------------
resource_id:              string ("GENERATOR:Meaningful ID")     N           *None*         `ResourceId`_                                                                                     
========================= ================================== ============ ================= ========================== =========================================

.. _Coefficients: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=polynomial#response-stage-polynomial-coefficient
.. _FrequencyLowerBound: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=polynomial#response-stage-polynomial-frequencylowerbound
.. _FrequencyUpperBound: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=polynomial#response-stage-polynomial-frequencyupperbound
.. _ApproximationLowerBound: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=polynomial#response-stage-polynomial-approximationupperbound
.. _ApproximationUpperBound: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=polynomial#response-stage-polynomial-approximationupperbound
.. _MaximumError: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=polynomial#response-stage-polynomial-maximumerror
.. _ApproximationType: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=polynomial#response-stage-polynomial-approximationtype
.. _ResourceID: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=polynomial#polynomial

==============================
JSON schema
==============================
`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/filter.schema.json>`_

==============================
Example
==============================

`_templates/TEMPLATE.polynomial.filter.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.polynomial.filter.yaml
  :language: YAML

==================
Class Navigation
==================

:ref:`Filter11` \<\=\= 

