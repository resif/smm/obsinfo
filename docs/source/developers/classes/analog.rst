
.. _Analog11:

****************
Analog
****************

===============
 Description
===============

StationXML does not specify Analog stages which do not have filters. They are
implemented here as a PZ filter without poles or zeroes.

---------------------
Python class:
---------------------

Analog

---------------------
 YAML / JSON label:
---------------------

ANALOG

------------------------------------------
Corresponding StationXML structure
------------------------------------------

PolesZeros (with no poles or zeros))

==============================
Object Hierarchy
==============================

-----------------------------------------
Superclass
-----------------------------------------

:ref:`PolesZeros <PolesZeros11>`

-----------------------------------------
Subclasses
-----------------------------------------

*None*

-----------------------------------------
Relationships
-----------------------------------------

* Is nested in :ref:`Stage <Stage>`

==============================
Attributes
==============================

======================== ================================== ============ ================= =============================== =======================================================================
        **Name**                   **Type**                 **Required**    **Default**    **Equivalent StationXML**            **Remarks**
------------------------ ---------------------------------- ------------ ----------------- ------------------------------- -----------------------------------------------------------------------
======================== ================================== ============ ================= =============================== =======================================================================

 
==============================
JSON schema
==============================

`<https://www.gitlab.com/resif/smm/obsinfo/-/tree/master/obsinfo/data/schemas/filter.schema.json>`_

==============================
Example
==============================

file: `_templates/analog.filter.yaml`

.. literalinclude:: ../../../../src/obsinfo/_templates/TEMPLATE.analog.filter.yaml
  :language: YAML

==================
Class Navigation
==================

:ref:`Filter11` \<\=\= 

