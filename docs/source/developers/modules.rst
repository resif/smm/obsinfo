Technical Documentation
=============================

.. toctree::
   :maxdepth: 4

   modules/obsinfo.obsmetadata_docs
   modules/obsinfo.subnetwork
   modules/obsinfo.instrumentation
   modules/obsinfo.misc
   modules/obsinfo.helpers
   modules/obsinfo.console_scripts
