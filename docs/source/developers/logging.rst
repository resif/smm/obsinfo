****************
Logging
****************

Logging is initiated  at the top of each file using 

.. code-block:: python

    import logging
    logger = logging.getlogger('obsinfo')

This activates the logger that is initiated by each console script and which
writes, by default, INFO level and above to a file in ~/.obsinfo/ and WARNING
level and above to the console.

Each console script should have the following at the top:

.. code-block:: python
    
  from ..helpers import init_logging
  logger = init_logging(name)

where ``name`` is the script name.  File logs will be written to 
~/.obsinfo/obsinfolog-{name}.

Here are the arguments for init_logging:

.. automodule:: obsinfo.helpers.init-logging
