obsinfo.obsmetadata package
===========================


obsinfo.obsmetadata module
--------------------------------------

.. automodule:: obsinfo.obsmetadata
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

