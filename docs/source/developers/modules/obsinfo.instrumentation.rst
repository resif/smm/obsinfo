obsinfo.instrumentation module
===============================


.. automodule:: obsinfo.instrumentation

Instrumentation class
----------------------------------------------

.. autoclass:: obsinfo.instrumentation.Instrumentation

Instrumentations class
----------------------------------------------

.. autoclass:: obsinfo.instrumentation.Instrumentations

Channel class
-----------------------------------------------

.. autoclass:: obsinfo.instrumentation.channel

Datalogger class
----------------------------------------------------

.. autoclass:: obsinfo.instrumentation.Datalogger

Sensor class
----------------------------------------------------

.. autoclass:: obsinfo.instrumentation.Sensor

Preamplifier class
----------------------------------------------------

.. autoclass:: obsinfo.instrumentation.Preamplifier

Stages class
-----------------------------------------------

.. autoclass:: obsinfo.instrumentation.Stages

Filter class
-------------------------------------

.. autoclass:: obsinfo.instrumentation.Filter

Equipment class
-----------------------------------------------

.. autoclass:: obsinfo.instrumentation.Equipment


Orientation class
-----------------------------------------------

.. autoclass:: obsinfo.instrumentation.Orientation
