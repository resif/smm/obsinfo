obsinfo.subnetwork package
==========================

Contains the main classes from Subnetwork to Station

.. automodule:: obsinfo.obsmetadata

obsinfo.subnetwork module
---------------------------------
.. automodule:: obsinfo.subnetwork
   :members:
   :undoc-members:
   :show-inheritance:

Subnetwork class
-----------------------------------
.. autoclass:: obsinfo.subnetwork.Subnetwork

Network class
--------------------------------
.. autoclass:: obsinfo.subnetwork.Network

Station class
---------------------------------
.. autoclass:: obsinfo.subnetwork.Station

Stations class
---------------------------------
.. autoclass:: obsinfo.subnetwork.Stations

Operator class
---------------------------------
.. autoclass:: obsinfo.subnetwork.Operator

Operators class
---------------------------------
.. autoclass:: obsinfo.subnetwork.Operators

Site class
---------------------------------
.. autoclass:: obsinfo.subnetwork.Site

Processing class
------------------------------------
.. automodule:: obsinfo.subnetwork.Processing
