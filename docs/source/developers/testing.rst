****************
Tests
****************

Rules
========

After every change to the code:

- Go to the directory ``{OBSINFOROOT}/tests`` and run ``pytest``

Before any deployment:

- go to the directory  ``{OBSINFOROOT}/src/obsinfo/tests`` and run ``pytest``
- verify that all of the instructions in the tutorial work

Explanation
------------------

- ``{OBSINFOROOT}/tests`` contains quick unit tests
- ``{OBSINFOROOT}/src/obsinfo/tests`` runs the ``schema``, ``print`` and ``
  ``plot`` commands on every ``example`` and ``template`` file
- Running the tutorial also verifies that the ``setup`` command works and that
  the examples provide the expected output
  
Coding recommendations
===========================

We implement testing using the unittest Unit testing framework
(https://docs.python.org/3/library/unittest.html). 
Unit tests are quick verifications of individual coding
units.  They should run quickly (< 1 second/test).
Each ``test_*.py`` file in the ``{OBSINFOROOT}/tests`` directory
corresponds to a single class or code file.
If you write a new function or method, write a unittest for it in
``{OBSINFOROOT}/tests``, either in an existing file or in a new one.
If you discover a bug that is not covered by a test, write a unittest for it in
``{OBSINFOROOT}/tests``.
