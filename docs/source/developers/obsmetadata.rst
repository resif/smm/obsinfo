****************
ObsMetadata
****************

``ObsMetadata`` is a dict subclass that allows some advanced operations useful
for obsinfo.  It has the additional methods:

- get_configured_element(): substitutes elements with selected or modification values
- get_information_file_format(): determines if an information file is in JSON or YAML format
- get_information_file_type(): determines the information file type ('network', etc)
- list_valid_types()
- read_json_yaml()
- read_json_yaml_ref()
- read_json_yaml_ref_datapath()
- validate()
- validate_date()
- validate_dates()

All but the first method seem like they should be in another class.  I'm guessing
Luis but them together to make sure that the output of any read was an ObsMetatdata
object, but that should be just as easy to do with another class....

This file is a stub.