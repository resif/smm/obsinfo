.. _Classes:

*********************
Classes
*********************

Information File Tree
----------------------------

network | :ref:`Network`
   * network | :ref:`FDSNNetwork`
   * operator | :ref:`Operator`
   * station | :ref:`Station`
      * processing | :ref:`ProcessingClockCorrections`
         * clock_correct_leap_second | :ref:`LeapSecond11`
         * clock_correct_linear_drift | :ref:`LinearDrift`
      * location | :ref:`Location1`
         * location_base | :ref:`LocationBase`
      * instrumentation(s) | :ref:`Instrumentation`
         * equipment | :ref:`Equipment`
         * channel | :ref:`Channel`
            * *No label* | :ref:`Instrument11`
               * *No label* | :ref:`InstrumentComponent`
                  * sensor | :ref:`Sensor`
                     * seed_codes | :ref:`SeedCodes11`
                  * preamplifier | :ref:`Preamplifier11`
                  * datalogger | :ref:`Datalogger11`
                  * stages | :ref:`Stages`
                     * stage | :ref:`Stage`
                     
                       * filter | :ref:`Filter11`
                       
                         * ADCONVERSION | :ref:`ADConversion11`    
                         * ANALOG | :ref:`Analog11`
                         * coefficients | :ref:`Coefficients11`
                         * DIGITAL | :ref:`Digital11`
                         * FIR | :ref:`FIR11`
                         * poles_zeros | :ref:`PolesZeros11`
                         * response_list | :ref:`ResponseList11`
                         * polynomial | :ref:`Polynomial11`

Names left of the ``|`` symbol are as they appear in information files as labels/keys. 
Names right of the ``|`` are the corresponding classes in the object model and the Python implementation of that model.
An empty label means the label does not exist in information files but exists as an object model / Python class.

Filter Types
---------------

        * type="ADCONVERSION" | :ref:`ADConversion11`
        * type="ANALOG" | :ref:`Analog11`
        * type="coefficients" | :ref:`Coefficients11`
        * type="DIGITAL" | :ref:`Digital11`
        * type="FIR" | :ref:`FIR11`
        * type="poles_zeros" | :ref:`PolesZeros11`
        * type="response_list" | :ref:`ResponseList11`
        * type="polynomial" | :ref:`Polynomial11`



.. toctree::
  :maxdepth: 1
  :caption: Table of Contents

  classes/subnetwork
  classes/network
  classes/operator
  classes/station
  classes/instrumentation
  classes/equipment
  classes/channel
  classes/instrument
  classes/instrument_component
  classes/sensor
  classes/seed_codes
  classes/preamplifier
  classes/datalogger
  classes/stages
  classes/stage
  classes/filter
  classes/AD_conversion
  classes/analog
  classes/coefficients
  classes/digital
  classes/FIR
  classes/poleszeros
  classes/response_list
  classes/polynomial
  classes/processing
  classes/linear_drift
  classes/leap_second
  classes/location
  classes/location_base
        

.. toctree::
  :maxdepth: 1
  :caption: Table of Contents (alphabetic)

  classes/AD_conversion
  classes/analog
  classes/channel
  classes/coefficients
  classes/datalogger
  classes/digital
  classes/equipment
  classes/filter
  classes/FIR
  classes/instrumentation
  classes/instrument_component
  classes/instrument
  classes/leap_second
  classes/linear_drift
  classes/location_base
  classes/location
  classes/network
  classes/operator
  classes/poleszeros
  classes/preamplifier
  classes/processing
  classes/response_list
  classes/stages
  classes/seed_codes
  classes/sensor
  classes/stage
  classes/station
