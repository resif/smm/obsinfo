****************
Add-ons
****************

The ``src/obsinfo/addons/`` directory is for command-line functions specific
to marine seismology facilities.  For now, it contains codes generating scripts
specific to the INSU-IPGP OBS facility: ``LC2SDS.py``, ``LCHEAPO.py`` and ``SDPCHAIN.py``
Let's look at the shorter of the two, ``LC2SDS.py`` which allows the extraction
of LC2000-format files to SeisComp Data Structure format.  In order to make it
run as a script, we add a hook in the ``setup.py`` file:

.. code-block:: python

    entry_points={
        'console_scripts': [
            'obsinfo=obsinfo.console_scripts.argparser:main',
            'obsinfo-test=obsinfo.tests.run_test_script:run_suite_info_files',
            'obsinfo-makescript_LC2MSpy=obsinfo.addons.LC2MS:console_lc2mspy',
            'obsinfo-makescript_LC2SDSpy=obsinfo.addons.LC2SDS:console_script'
        ]
    },

The last ``console_scripts`` line links the script name ``obsinfo-makescripts_LC2SDS``
to the function ``_console_script()`` in ``LC2SDS.py``

When we run ``obsinfo-makescripts_LC2SDS`` on an appropriate subnetwork file,
we get:

.. code-block:: console

    > obsinfo-makescripts_LC2SDS MOMAR_2017-2018_K.INSU-IPGP.v1.subnetwork.yaml /Volumes/PARC_OBS_INSU/DATA_SMM/2007-.EMSO_AZORES/2017-2018.MOMAR_K.suitable 
    Creating LC2SDS_weak process script, file_type='subnetwork'
    network 4G, stations LSVNK, LSVWK, LSVCK, LSVEK, LSVSK, LSVHK

and the output script, named ``process_LC2SDS.sh`` contains:

.. code-block:: bash

    #!/bin/bash

    DATA_DIR=/Volumes/PARC_OBS_INSU/DATA_SMM/2007-.EMSO_AZORES/2017-2018.MOMAR_K.suitable

    run_station () {
        # Run lcfix and lc2SDS_py for one station
        # $1: network code
        # $2: station code
        # $3: obs type
        # $4: lc2SDS_py command-line timing options
        echo "Working on station $2"
        STATION_DIR=$DATA_DIR/$2
        echo "------------------------------------------------------------"
        echo "Running LCFIX"
        mkdir $STATION_DIR/lcheapo_fixed
        command cd $STATION_DIR
        lchfiles=$(command ls *.lch)
        command cd -
        echo "lchfiles:" $lchfiles
        lcfix $lchfiles -d "$STATION_DIR" -o "lcheapo_fixed"
        echo "------------------------------------------------------------"
        echo "Running lc2SDS_py"
        mkdir -p $STATION_DIR/../
        command cd $STATION_DIR/lcheapo_fixed
        lchfiles=$(command ls *.fix.lch)
        command cd -
        echo "lchfiles:" $lchfiles
        cmd="lc2SDS_py $lchfiles -d \"$STATION_DIR\" -i \"lcheapo_fixed\" -o \"../\" --network \"$1\" --station \"$2\" --obs_type \"$3\" $4"
        echo "Running: $cmd"
        eval $cmd
        echo "------------------------------------------------------------"
        echo "Removing intermediate files"
        command rm -r $STATION_DIR/lcheapo_fixed
    }

    run_station "4G" "LSVNK" "SPOBS2" "--start_times \"2017-07-17T11:02:00Z\" \"2017-07-17T11:02:00Z\" --end_times \"2018-08-11T18:03:40.4518Z\" \"2018-08-11T18:03:41Z\""
    run_station "4G" "LSVWK" "SPOBS2" "--start_times \"2017-07-13T10:21:00Z\" \"2017-07-13T10:21:00Z\" --end_times \"2018-08-13T09:55:00.4594Z\" \"2018-08-13T09:55:00Z\""
    run_station "4G" "LSVCK" "SPOBS2" ""
    run_station "4G" "LSVEK" "SPOBS2" "--start_times \"2017-07-15T08:59:00Z\" \"2017-07-15T08:59:00Z\" --end_times \"2018-08-11T15:50:00.5130Z\" \"2018-08-11T15:50:00Z\""
    run_station "4G" "LSVSK" "SPOBS2" ""
    run_station "4G" "LSVHK" "HYDROCT1" ""

The ``run_station()`` function is boilerplate: the information extracted from the
subnetwork file is in the ``run_station`` calls at the bottom of the file.

Let's look at the code that creates this:

The ``_console_script()`` function calls a function to define and parse the
command-line arguments, then reads in the subnetwork file, calls the function
``process_script()`` to create the script text, then writes the output to a file:

.. literalinclude:: ../../../src/obsinfo/addons/LC2SDS.py
    :language: python
    :pyobject: _console_script
    
All of the station information is passed to ``process_script()`` in its second
argument (``subnetwork.stations``), which is a list of obsinfo ``Station`` objects.

.. literalinclude:: ../../../src/obsinfo/addons/LC2SDS.py
    :language: python
    :pyobject:  process_script
    
``process_script()`` is rather short: it outputs the header, then
the boilerplate ``run_station()`` function.  Then, for each station, it calls
the function ``_run_station_call()``

.. literalinclude:: ../../../src/obsinfo/addons/LC2SDS.py
    :language: python
    :pyobject:  _run_station_call

``run_station_call()`` gets the station code, the obs type, the clock drift and
leapsecond information, and puts it all out on a line that calls ``run_station()``.
These are the bottom lines in the ``process_LC2SDS.sh`` file