****************
Introduction
****************

#########################
Python architecture
#########################


Package and Modules
=====================

The package name is ``obsinfo``

``obsinfo/console_scripts/`` contains the ``obsinfo`` command-line
codes.  In this directory, ``argparser.py`` reads the command and
and dispatches the result to the ``{subcommand}.py`` code.

``obsinfo/subnetwork/`` and  ``obsinfo/instrumentation/`` contain the main codes
defining the ``obsinfo`` classes.

``obsinfo/helpers/`` contains classes and routines that are used by the codes
in both the ``obsinfo/subnetwork`` and  ``obsinfo/instrumentation`` directories.

``obsinfo/misc/`` contains functions and classes used by other codes.
It's not clear what is different from ``obsinfo/helpers/``, if it's not that
the ``misc`` entities are more often called from the ``addons`` codes.

``obsinfo/obsmetadata/`` contains code to read and validate information files.

``obspy/addons/`` contains modules specific to proprietary systems:

- ``obspy.addons.LC2MS`` creates scripts to convert LCHEAPO OBS data to
  miniSEED using the ``lc2ms`` software
- ``obspy.addons.SDPCHAIN`` creates scripts to convert basic miniSEED data
  to OBS-aware miniSEED using the ``SDPCHAIN`` software suite
- ``obspy.addons.OCA`` creates JSON metadata in a format used by the
  Observatoire de la Cote d'Azur to create StationXML


Auxiliary subdirectories
===========================

``obsinfo/data/schema/``
------------------------------------------------------------

Contains JSON Schema for each file type.  Used by most of the command-line
tools.


``obsinfo/_examples/``
------------------------------------------------------------

Contains example information files and scripts.  The contents of this
directory can be copied to a user-specified location to make a working,
modifiable database using ``obsinfo setup``:

- ``_examples/instrumentation_files`` contains a complete set of information files

  * ``_examples/instrumentation_files/network`` contains **network**  files

  * ``_examples/instrumentation_files/instrumentation`` contains **instrumentation**,
    **instrument_components**, **response** and **filter** files.

- ``_examples/scripts`` contains bash scripts to look at and manipulate these files
  using the executables.  Running these scripts is a good way to make sure your
  installation works, looking at the files they work on is a good way to start
  making your own information files.

``obsinfo/_templates/``
------------------------------------------------------------

Contains templates for every type of information file.  They can be
copied to a user-specified location using ``obsinfo template``, are fully
functional and contain every element of the file type, plus comments indicating
which elements are optional.

``obsinfo/_stationxml-validator/``
------------------------------------------------------------

Contains java stationxml-validator code, allowing a validation of
created files.  In the future, the path to this code should be included
in the .obsinforc file, allowing the user to substitute a newer
stationxml-validator.

``obsinfo/tests/``
------------------------------------------------------------

Contains tests that validate the ``_example``  and ``_templates`` files, using
the ``pytest`` command.
Runs ``obsinfo schema``, ``obsinfo print`` and ``obsinfo plot`` on each file, so
running  takes a long time.
Should be run before each deployment of a new version.

``obsinfo/_instrumentation_databases/``
------------------------------------------------------------

Contains instrumentation databases for different parks.  This is not
meant to be distributed with the code.

Comments on versioning
=======================

We use standard MAJOR.MINOR.MAINTENANCE version numbering:

- MAJOR increments every time the information 
  file structure changes in a **non-backwards-compatible** way

- MINOR increments every time a new feature is added that does not break
  existing functioning

- MAINTENANCE increments for each bugfix or code improvement that does not
  change anything externally (except improved warnings/errors/prints/plots).