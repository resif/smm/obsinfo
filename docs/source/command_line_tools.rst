.. _command_line_tools:


**************************
Command-line tools
**************************

**obsinfo** is the main command-line tool.
It has several subcommands, which you can see by typing ``obsinfo -h``:

============================  =============================================
subcommand                    Description
============================  =============================================
``version``                   Print obsinfo version
``setup``                     Set up obsinfo environment
``schema``                    Validate an information file against its schema
``print``                     Print the obsinfo class created by a file
``plot``                      Plot information in the given file
``xml``                       Create a stationxml file from a subnetwork file
``template``                  Output an information file template
``configurations``            Print obsinfo information file configurations
============================  =============================================

For help on a given subcommand, type ``obsinfo {subcommand} -h``

================================
Running on multiple files
================================
The subcommands ``schema``, ``print``, ``plot``, ``xml`` and ``configurations``
process information files. All but ``xml`` can also work on entire directories
of files.  For example, to validate one file against schema, you would write:

.. code-block:: console

    > obsinfo schema {NAME}.{type}.yaml

To validate all of the information files in a directory against their schemas, use:

.. code-block:: console

    > obsinfo schema {DIRECTORY_NAME}

To validate all of the information files in a directory and its subdirectories, use:

.. code-block:: console

    > obsinfo schema --drilldown {DIRECTORY_NAME}

To validate all of the information files in the DATAPATH and its subdirectories, use:

.. code-block:: console

    > obsinfo schema DATAPATH


================================
Subcommands
================================

``version``
================================
Prints the version number

``setup``
================================

Creates an ``.obsinforc`` file that indicates where obsinfo should look for
the information files (see :ref:`file_discovery`), and optionally creates a directory with example
information files.
 
The standard call is:

.. code-block:: console

    > obsinfo setup -D DATABASE
    
which will create a directory named `DATABASE/`, containing "starter"
obsinfo files, in the current directory

``schema``
================================

Validates the specified information file against it's JSON schema.
The standard calls are:

.. code-block:: console

    > obsinfo schema {NAME}.{type}.yaml
    > obsinfo schema {DIRECTORY_NAME}
    > obsinfo schema --drilldown {DIRECTORY_NAME}
    > obsinfo schema DATAPATH


``print``
================================

Reads an information file into obsinfo and prints obsinfo's string representation.
The standard calls are:

.. code-block:: console

    > obsinfo print {NAME}.{type}.yaml
    > obsinfo print {DIRECTORY_NAME}
    > obsinfo print --drilldown {DIRECTORY_NAME}
    > obsinfo print DATAPATH
      

``plot``
================================

Plots a representation of the parameters in an information file.  The plot
type depends on the information file type:

============================================  ====================================================================
Information file type                         Plot type
============================================  ====================================================================
``subnetwork``                                Station map; Data time extent
``instrumentation_base``                      Instrument responses for each channel
``datalogger``, ``preamplifier``, ``sensor``  Instrument response
``stages``                                    Instrument response
``filter``                                    Instrument response; Impulse response if ``FIR`` or ``Coefficients``
============================================  ====================================================================

Other information file types cannot be plotted.

The standard calls are:

.. code-block:: console

    > obsinfo plot {NAME}.{type}.yaml
    > obsinfo plot {DIRECTORY_NAME}
    > obsinfo plot --drilldown {DIRECTORY_NAME}
    > obsinfo plot DATAPATH
    
      
``xml``
================================

Creates a StationXML from a subnetwork file.  The standard call is:

.. code-block:: console

    > obsinfo xml {NAME}.subnetwork.yaml
 
.. _obsinfo_template:

``template``
================================

Copies a template file to the specified directory.  The standard call is:

.. code-block:: console

    > obsinfo template {file_type}

and file types are :ref:`datalogger_base_template`,
:ref:`instrumentation_base_template`,
:ref:`location_base_template`,
:ref:`network_template`,
:ref:`operator_template`,
:ref:`person_template`,
:ref:`preamplifier_base_template`,
:ref:`sensor_base_template`,
:ref:`stage_base_template`,
:ref:`subnetwork_template`,
:ref:`timing_base_template`,
:ref:`ad_conversion_filter_template`,
:ref:`fir_filter_template`,
:ref:`analog_filter_template`,
:ref:`coefficients_filter_template`,
:ref:`digital_filter_template`,
:ref:`poleszeros_filter_template`, and
:ref:`response_list_filter_template`

 
``configurations``
================================

Plots all configurations for an information ``*_base`` file.
The standard calls are:

.. code-block:: console

    > obsinfo configurations {NAME}.{type}.yaml
    > obsinfo configurations {DIRECTORY_NAME}
    > obsinfo configurations --drilldown {DIRECTORY_NAME}
    > obsinfo configurations DATAPATH
 

================================
Usage
================================

The ``setup`` subcommand is used to set up your initial configuration (location
of the "datapath" information files, creation of the logging directory), or to modify
that configuration.

The ``schema``, ``print`` and ``plot`` subcommands are useful for validating files
before you create a StationXML file using the ``xml`` subcommand.
You should run then in order:

1) ``schema``, which validates the file against its JSON Schema file
2) ``print``, which reads the information into obsinfo classes
   and then prints the class description string
3) ``plot``, to verify that the parameters you entered make sense.

The ``template`` and ``configurations`` subcommands are useful for creating
new information files.  ``template`` creates a commented file with all possible
elements.  ``configurations`` indicates all the specified configurations
for a ``*_base`` file.

The ``xml`` subcommand creates StationXML files.

================================
Add-ons
================================

There are some add-on commands for applying ``obsinfo`` to facility-specific tasks.
As of November 2024, they are:

+----------------------------------+--------------------------------------------+
| command                          |  description                               |
+==================================+============================================+
| ``obsinfo-makescripts_LC2SDSpy`` | Output a script to extract LCHEAPO data to |
|                                  | SeisComp Data Structure ("client format")  |
|                                  | using the ``lcheapo`` python package       |
+----------------------------------+--------------------------------------------+
| ``obsinfo-makescripts_LC2MSpy``  | Output a script to extract LCHEAPO data to |
|                                  | single-channel miniSEED files ("A-node     |
|                                  | format") using the ``lcheapo`` package     |
+----------------------------------+--------------------------------------------+
