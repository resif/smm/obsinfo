.. _InstallStartup: 

***********************************
Installation
***********************************

==========
Setting up
==========

The following are basic steps to install and confirm that everything is working

Installing
=====================================================

.. note::  **obsinfo** does not work in Windows: use a Mac or Linux computer

- Install obspy using their `Conda Installation instructions`_
- In your obspy environment, install obsinfo by typing ``pip install obsinfo==1.0b3``
- type ``pip list`` and ``obsinfo version`` to confirm that the version number is correct 

.. _copy_example_database:

Copy an example database into your own folder
=====================================================

- Create a working directory
- Go in there and run ``obsinfo setup -d DATABASE``

A subfolder named ``DATABASE`` will be created.
Inside will be a directory named ``instrumentation_files`` and under that
will be many subdirectories with  :ref:`information files`.

Create a StationXML file
=====================================================

Create a subnetwork file in your working directory:

.. code-block:: bash

    obsinfo template subnetwork
    
Then run ``obsinfo xml`` on it:

.. code-block:: bash

    obsinfo xml TEMPLATE.subnetwork.yaml
    
A StationXML file named "TEMPLATE.station.xml" should be created

*You can also copy one of the example ``subnetwork`` files from your*
``DATABASE/subnetwork_files/``
*directory to your working directory, and work on it.*
    
Test the other command-line codes
=====================================================

Try the following lines, to verify that the other subcommands work:

.. code-block:: bash

    > obsinfo schema TEMPLATE.subnetwork.yaml
    > obsinfo print TEMPLATE.subnetwork.yaml
    > obsinfo plot TEMPLATE.subnetwork.yaml

.. _Conda Installation instructions: https://github.com/obspy/obspy/wiki/Installation-via-Anaconda


