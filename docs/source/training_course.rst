***************
Training Course
***************

.. toctree::
  :caption:  Table of contents
  :maxdepth: 2

  training_course/1_day1
  training_course/setup
  training_course/2_day2
  training_course/3_create_your_own
  training_course/4_future
  