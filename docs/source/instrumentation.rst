.. _Instrumentation_Files:

***********************************************
Instrumentation Files
***********************************************


Instrumentation files define the components and instrument responses of
instrumentations.  They should be created by the technical staff responsable
for the instrumentations.  Once created, they can be used for many different
``subnetwork`` files.  The ``instrumentation`` structure consist of several levels,
most of which point to other files.

========================================
Example ``instrumentation_base`` files
========================================


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/instrumentation_bases/EXAMPLE_noconfigs.instrumentation_base.yaml
    :language: yaml
    :start-after: instrumentation_base
    :caption:

This simple ``instrumentation_base`` file shows the basic elements of an
instrumentation.  The details of its main components: a datalogger, a preamplifier
and a sensor, are specified in separate ``datalogger_base``, ``preamplifier_base``
and ``sensor_base`` files, which makes the ``instrumentation_base`` file
clearer to read and allows the components to be used in other instrumentations.

Below is an ``instrumentation_base`` file which defines several configurations

.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/instrumentation_bases/EXAMPLE.instrumentation_base.yaml
    :language: yaml
    :start-after: instrumentation_base
    :caption:
    
The default configuration, ``seismometer_sn_1-399_2012+``, is the most commonly
used.  The others allow the user to specify all possible configurations,
according to the sensor sphere number and the deployment epoch.

========================================
Lower-level files
========================================

Here are details of the **instrument_component** files called by the above
**instrumentation** files.

The ``datalogger_base`` file and its subfiles
===========================================================

.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/datalogger_bases/EXAMPLE.datalogger_base.yaml
    :language: yaml
    :start-after: datalogger_base
    :caption:
    
There are 49  stage declarations, but only 3 unique ``stage_bases``,
so it is more efficient to declare them in separate files:


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/datalogger_bases/stage_bases/EXAMPLE_FIR1.stage_base.yaml
    :language: yaml
    :start-after: stage_base
    :caption:
    
The above top-level FIR is an ``ADConversion`` filter, which is declared
within the ``stage_base`` file.


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/datalogger_bases/stage_bases/EXAMPLE_FIR2.stage_base.yaml
    :language: yaml
    :start-after: stage_base
    
The above second-level FIR is repeated across multiple stages during
data decimation.  Its filter is has relatively few coefficients, so it
is declared within the ``stage_base`` file.`


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/datalogger_bases/stage_bases/EXAMPLE_FIR3.stage_base.yaml
    :language: yaml
    :start-after: stage_base
    :caption:

    
The above third-level FIR has a much longer FIR filter, which is therefore
declared in a separate file that we will not show here.


The ``preamplifier_base`` file and its subfiles
===========================================================


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/preamplifier_bases/EXAMPLE_BBOBS.preamplifier_base.yaml
    :language: yaml
    :start-after: preamplifier_base

This above preamplifier has one stage, which is declared in a separate file, below:


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/preamplifier_bases/stage_bases/EXAMPLE_BBOBS.stage_base.yaml
    :language: yaml
    :start-after: stage_base
    :caption:
    
The stage above has no filtering, just a gain.  We could have declared
it directly in the ``preamplifier_base`` file.

The seismometer ``sensor_base`` file and its subfiles
===========================================================


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/sensor_bases/EXAMPLE_BBSeismometer.sensor_base.yaml
    :language: yaml
    :start-after: sensor_base
    :caption:

The above sensor has a single stage, which is read from a separate file.  It has many
configurations, allowing the user to obtain the correct sensor serial number and
response by specifying the deployment sphere number.

.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/sensor_bases/stage_bases/EXAMPLE_BBSeismometer.stage_base.yaml
    :language: yaml
    :start-after: stage_base
    :caption:

This response stage has two possible filters, depending on the serial number, and
four possible gains, depending on the serial number and how the sensor
is connected to the datalogger (single-sided or differential)

Below we show the filters for T240 serial numbers 1-399 and for serial numbers 400+.
The coefficients are slightly different

.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/sensor_bases/stage_bases/filters/EXAMPLE_BBSeismometer_SN1-399.filter.yaml
    :language: yaml
    :start-after: filter
    :caption:


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/sensor_bases/stage_bases/filters/EXAMPLE_BBSeismometer_SN400-.filter.yaml
    :language: yaml
    :start-after: filter
    :caption:

The pressure ``sensor_base`` file and its subfiles
===========================================================


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/sensor_bases/EXAMPLE_DPG.sensor_base.yaml
    :language: yaml
    :start-after: sensor_base
    :caption:

The sensor has a single stage, which is read from a separate file.  The ``sensor_base``
file specifies several configurations, allowing the right sensor serial number and
response to be put into the StationXML file simply by specifying the
appropriate configuration.


.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/sensor_bases/stage_bases/EXAMPLE_DPG.stage_base.yaml
    :language: yaml
    :start-after: stage_base
    :caption:


