.. _Tutorial6:

********************************************
Building a datalogger information file
********************************************

Dataloggers are the components used to record the data treated by the
preceding sensor and preamplifier stages.
Their configuration files may be quite long due to the number of necessary stages.

Dataloggers have the elements common to any instrument component,
plus two extra elements: ``correction`` and ``sample_rate``.

**sample_rate**

is the instrument's sample rate.

**correction**

is the time shift applied to correct
for any delay introduced by the instrument stages.  The sign convention is the
opposite of that for delays: a positive value indicates that the trace was corrected
to an earlier time (see `StationXML Correction`_)

.. _StationXML Correction: http://docs.fdsn.org/projects/stationxml/en/latest/reference.html?highlight=correction#correction-required

All stages can have delays, which should be
corrected. The ``correction`` field indicates that this was done.
In StationXML **correction** is an attribute of each stage.
However, as most delays come from the datalogger's digital filters, where they
are in samples which can only be converted to time once the stages sampling
rates are known, **obsinfo** requires you to specify the correction at the
``datalogger`` level. Two processes are allowed: 

#. Set the correction equal to the delay in every stage.
   This is the most common use case for commercial dataloggers but it is
   misleading if the datalogger doesn't REALLY correct time at every stage.
#. Set the correction equal to zero in all stages except the last, where the correction
   is set to a value provided by the user.
   This is what most non-commercial dataloggers do (and maybe commercial too?).
    
The first case is activated by **NOT** specifying the ``correction`` in the
datalogger information file.
In other words, **obsinfo** assumes by default a "perfect" delay correction.
Note that this will also be applied to the non-datalogger stages, if they
have a specified delay

The second case is activated by specifying a ``correction`` in the datalogger
information file.
If your datalogger does not correct for the digital delay, specifying
``correction: 0`` does the right thing, which is to set
``correction=0`` in every stage.`


Datalogger configuration definitions
---------------------------------------

The code below is a datalogger_base information file.
Each configuration corresponds to a different sample rate which, for this
particular datalogger, corresponds to a different number of FIR decimation
filters. The first stage, FIR1, always outputs a 32000 sample rate signal.
FIR2 and FIR3 are factor 2 decimation filters.
There is only one FIR3 filter, the last stage, and the number of FIR2 filters
is chosen to arrive at the desired output sample rage.
For example, a 1000 sps output sample rate requires four FIR2s in addition to the
FIR3 (2^5 = 32).  Slower sample rates simply add more FIR2s.

.. code-block:: yaml

  ---
  datalogger_base:
    equipment:
        model: "CS5321/22"
        type: "delta-sigma A/D converter + digital filter"
        description: "CS5321/22 delta-sigma A/D converter + FIR digital filter"
        manufacturer: "Cirrus Logic"
        vendor: "various"
    configuration_default: "125 sps"
    configurations:
        "62.5sps":
            sample_rate: 62.5
            correction: 0.464
            stages:
                - $ref: "stages/CS5321_FIR1.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR3.stage_base.yaml#stage_base"
        "125sps":
            sample_rate: 125
            correction: 0.232
            stages:
                - $ref: "stages/CS5321_FIR1.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR3.stage_base.yaml#stage_base"
        "250sps":
            sample_rate: 250
            correction: 0.116
            stages:
                - $ref: "stages/CS5321_FIR1.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR3.stage_base.yaml#stage_base"
        "500sps":
            sample_rate: 500
            correction: 0.058
            stages:
                - $ref: "stages/CS5321_FIR1.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR3.stage_base.yaml#stage_base"
        "1000sps":
            sample_rate: 1000
            correction: 0.029
            stages:
                - $ref: "stages/CS5321_FIR1.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR2.stage_base.yaml#stage_base"
                - $ref: "stages/CS5322_FIR3.stage_base.yaml#stage_base"

Configuration labels are flexible and can suit any purpose desired by the user.
The best practice is to keep them short, explicit and consistent among different
configurations for the same instrumentation/component/stage.

Next we will see stages and filters in more detail.
