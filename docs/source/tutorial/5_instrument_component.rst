.. _Tutorial5:

********************************************************************************************
Building instrument components
********************************************************************************************

Sensors, preamplifiers and dataloggers are all instrument components.
All instrument components have an ordered list of response stages.
StationXML has a single ordered list of response stages, which is constructed
from:

#. the ``sensor`` stages
#. the ``preamplifier`` stages (if any)
#. the ``datalogger`` stages.

As with all modifiable elements, instrument components source filenames end
with ``_base`` and they allow a ``configurations`` element.

The simplest way to build an instrument_component is from a copy of an
existing component, or using the ``template`` subcommand.

``sensor_base`` template
=================================

.. code-block:: console

    > obsinfo template sensor_base


.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.sensor_base.yaml
    :language: yaml
    
Running the ``schema``, ``print`` and ``plot`` subcommands on this file returns:

.. code-block:: console

    Validating sensor_base file
    Reading /Users/crawford/temp/TEMPLATE.sensor_base.yaml
    schema =   sensor_base.schema.json
        Testing instance ...OK
    sensor_base test for: /Users/crawford/temp/TEMPLATE.sensor_base.yaml: PASSED
    
.. code-block:: console
    
    TEMPLATE.sensor_base.yaml: 
        Sensor:
            band: broadband
            instrument code: H
            equipment: Equipment:
                type: Broadband seismometer
                description: Triaxial force-feedback seismometer [config: Default configuration]
                model: CMG-3T
                manufacturer: Guralp, Ltd
                vendor: various
                serial_number: 2014a2
                resource_id: IPGP:2004iepw44
                installation_date: 2024-09-30
                removal_date: 2025-08-31
                calibration_dates: OIDates: 2 OIDates
            stages: Stages: [Stage [config: SN1-399, differential]]


.. image:: images/obsinfo_plot_sensor_base.png
    
``preamplifier_base`` template
=================================

.. code-block:: console

    > obsinfo template preamplifier_base


.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.preamplifier_base.yaml
    :language: yaml
    
    
.. code-block:: console
    
    > obsinfo print TEMPLATE.preamplifier_base.yaml 
    TEMPLATE.preamplifier_base.yaml: 
        Preamplifier:
            equipment: Equipment:
                type: Analog gain/filter card
                description: CUBI gain/filter card, LP at ~100 MHz [config: 128x gain]
                model: CUBI-RC-RC
                manufacturer: IPGP
                vendor: None
                serial_number: 2014a2
                resource_id: IPGP:2004iepw44
                installation_date: 2024-09-30
                removal_date: 2025-08-31
                calibration_dates: OIDates: [2004-09-22T07:00:00]
            stages: Stages: [Stage [config: 1.0x]]


.. code-block:: console
    
    > obsinfo plot TEMPLATE.preamplifier_base.yaml 

.. image:: images/obsinfo_plot_preamplifier_base.png

We see the gain of 200, which was forced by the optional ``stage_modifications``
element in the template file.  And that there was no frequency-based filtering.

``datalogger_base`` template
=================================

.. code-block:: console

    > obsinfo template datalogger_base


.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.datalogger_base.yaml
    :language: yaml
    
.. code-block:: console
    
    > obsinfo print TEMPLATE.datalogger_base.yaml
    TEMPLATE.datalogger_base.yaml: 
        Datalogger:
            sample_rate: 100
            correction: 27.5
            equipment: Equipment:
                type: delta-sigma A/D converter + digital filter
                description: Single Chip High-Resolution Analog-to-Digital Converter [config: 100 sps, linear phase FIR]
                model: ADS1281
                manufacturer: Texas Instruments
                vendor: various
                serial_number: 2014a2
                resource_id: IPGP:2004iepw44
                installation_date: 2024-09-30
                removal_date: 2025-08-31
                calibration_dates: OIDates: 2 OIDates
            stages: Stages:
                - Stage "EXAMPLE FIR1"
                - Stage "EXAMPLE FIR2"
                - Stage "EXAMPLE FIR2"
                - Stage "EXAMPLE FIR2"
                - Stage "EXAMPLE FIR2"
                - Stage "EXAMPLE FIR2"
                - Stage "EXAMPLE FIR2"
                - Stage "EXAMPLE FIR3"
                

.. code-block:: console
    
    > obsinfo plot TEMPLATE.datalogger_base.yaml 

.. image:: images/obsinfo_plot_datalogger_base.png

We see the effect of the cutoff filter at the Nyquist frequency, and
of forcing the gain value to 200.