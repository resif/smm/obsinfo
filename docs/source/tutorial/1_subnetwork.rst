.. _JREF: https://datatracker.ietf.org/doc/html/draft-pbryan-zyp-json-ref-03
.. _tutorial_subnetwork:


***********************************************
Building a subnetwork file
***********************************************

Setup
_____________________________________


Before runnning this tutorial, install **obsinfo** and run 
``obsinfo setup``, as explained in :ref:`InstallStartup`.


Creating a subnetwork file
_____________________________________

The easiest way to create a subnetwork file is from a template.
By typing ``obsinfo template subnetwork``, you will create the following file,
named ``TEMPLATE.subnetwork.yaml``.


.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.subnetwork.yaml
    :language: yaml
    
*(Alternatively, you can copy one of the ``DATAPATH/subnetwork_files/EXAMPLE_*.subnetwork.yaml`` files
to your working directory)*

``TEMPLATE.subnetwork.yaml`` contains `JREF`_ links to information
files that are found in the example database: for this tutorial, your DATAPATH
should point there.  If you installed as instructed, this should already be
the case.  If not, see :ref:`copy_example_database`.

.. _validating_subnetwork_file:

Validating your subnetwork file
_____________________________________

Run the ``schema`` subcommand
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Type ``obsinfo schema TEMPLATE.subnetwork.yaml``.  The console output should be:

.. code-block:: console

    Validating subnetwork file
    Reading {somepath}/TEMPLATE.subnetwork.yaml
    schema =   subnetwork.schema.json
        Testing instance ...OK
    subnetwork test for: {somepath}/TEMPLATE.subnetwork.yaml: PASSED

Where ``{somepath}`` is the full path to the current directory

Run the ``print`` subcommand
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Type ``obsinfo print TEMPLATE.subnetwork.yaml``.  The console output should be:

.. code-block:: console

    Subnetwork:
        references:
            operator: MY_OPERATOR_NAME
            campaign: MY_CAMPAIGN_NAME
        network: Network:
            code: XX
            name: EXAMPLE NETWORK
            operators: Operators: []
            start_date: 2011-01-01
            end_date: 2016-12-31
            description: The code XX is reserved for test or transient data
            restricted_status: None
            identifiers: Identifiers: []
        extras: List of strings:
        - example: an example
        - example_list: [1, 2, 3, 4, 5]
        stations_comments: List of strings:
        - Comment:
            value: This is a simple network-level comment
        - Comment:
            value: This is a full network-level comment
            authors: Persons: [Person ["Teddy Riner"]]
            begin_effective_time: 2022-01-02T00:00:00
            end_effective_time: 2022-01-03T00:00:00
            id: 4000
            subject: comment subject
        - Comment:
            value: Extra attributes: ["example: an example", "example_list: [1, 2, 3, 4, 5]"]
        stations_operators: Operators: [agency: Example marine seismology instrumentation operator]
        stations: Stations: [Station STA1]


If you type ``obsinfo print TEMPLATE.subnetwork.yaml -n 2``, you will get two
levels of detail: notably, the details of ``STA1``.


Run the ``plot`` subcommand
~~~~~~~~~~~~~~~~~~~~~~~~~~~


Type ``obsinfo plot TEMPLATE.subnetwork.yaml``.  The program will plot
a station map:

.. image:: images/obsinfo_plot_subnetwork_stationmap.png

and the channel time spans:

.. image:: images/obsinfo_plot_subnetwork_timespans.png

The time span for ``*.CH1`` is shorter than the others, as specified in
``TEMPLATE.subnetwork.yaml```.

Run the ``xml`` subcommand
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Type ``obsinfo xml TEMPLATE.subnetwork.yaml``  The console output will be:

.. code-block:: console

    Reading subnetwork file...Creating Subnetwork object...[station][to_obspy()] [WARNING ] obspy 1.4.0 could not create station-level external_references, so your's will not be written. If this has been fixed in obspy, put an issue on the obsinfo gitlab page so that we can update this
    Network XX (EXAMPLE NETWORK - The code XX is reserved for test or transient data)
        Station Count: 1/None (Selected/Total)
        2011-01-01T00:00:00.000000Z - 2016-12-31T00:00:00.000000Z
        Access: UNKNOWN
        Contains:
            Stations (1):
                XX.STA1 (West volcano flank)
            Channels (4):
                XX.STA1.00.CDG, XX.STA1.00.CHZ, XX.STA1.00.CH1, XX.STA1.00.CH2
    StationXML file created: TEMPLATE.station.xml
    Running /Users/crawford/_Work/Programming/obsinfo/src/obsinfo/stationxml-validator/stationxml-validator-1.7.5.jar ... 112,Error,XX,,,,2011-01-01T00:00:00,2016-12-31T00:00:00,Sta: STA1 endDate 2022-07-01T01:00:00 cannot occur after network endDate 2016-12-31T00:00:00

The program creates a StationXML file named ``TEMPLATE.station.xml``,
and validates it using ``stationxml-validator``.

Your turn!
~~~~~~~~~~~~~~~~~~~~~~~

You can modify the subnetwork file and see what the outputs are.  Some ideas
are:

- Remove all of the elements between the `# OPTIONAL ...` and `# END OPTIONAL ...`
  comments.  This will make a much shorter ``subnetwork`` file with only
  essential information.
- Copy the ``STA1`` element a few times and change the copies'
  name, locations and dates.  ``obsinfo plot`` will reflect the changes.
  
In general, use the subcommand sequence:

1. ``obsinfo schema``
2. ``obsinfo print``, 
3. ``obsinfo plot``
4. ``obsinfo xml``

to check your information files from the most basic (schema checking) to the
highest (StationXML creation) levels, with the ``plot`` command letting you check
that the station positions and active times are what you wanted.

Relevant references:
_____________________________________

 Details about some of the concepts are found in:

- :ref:`yaml_files`
- :ref:`information_files`
- :ref:`Subnetwork_Files`
- :ref:`Base-configuration-modification elements <advanced_base_configuration_modifications>`
- :ref:`advanced_channel_modifications`
.. :ref:`file_discovery``

