
.. _Tutorial8:

******************************
Building a filter file
******************************

As pointed out in the previous section, all stages have an associated
"filter", even if no filtering is performed.
The filters specified in StationXML are:

* PolesZeros_ - Any analog filter specified by its poles and its zeros.
  Use this for Laplace transforms and IIR filters.
* FIR_ - Finite Input Response digital filter
* Coefficients_ - A more general way of expressing a digital filter.
  Can be used to express a FIR or an IIR filter.
* ResponseList_ - A filter whose response is expressed as a list of 
  frequency, amplitude and phase.
* **Polynomial** - A filter expressed as a polynomial. 

All except **Polynomial** are available in **obsinfo**.

**obsinfo** also offers some streamlined filter types for stages which have a simpler
or different representation than the StationXML.  They are stored as StationXML
filters, but you have less (or more pertinent) information to enter: 

* ADCONVERSION_ - Analog to digital conversion stage.  Acts as a
  Coefficients_ filter without any coefficients, and allows you to enter
  the maximum and minimum voltage and counts values, calculating the gain for
  you
* ANALOG_ - gain-only Analog stage
* DIGITAL_ -  gain-only Digital stage

Below we provide a link to the template file for each filter and show the
result of ``obsinfo plot`` on each of them:

PolesZeros
-------------------------

:ref:`Template <poles_zeros_filter_template>`

.. image:: images/obsinfo_plot_filter_poleszeros.png

FIR
-------------------------

:ref:`Template <fir_filter_template>`

  .. image:: images/obsinfo_plot_filter_FIR.png
  .. image:: images/obsinfo_plot_filter_FIR_coeffs.png

Coefficients
-------------------------

:ref:`Template <coefficients_filter_template>`

  .. image:: images/obsinfo_plot_filter_coefficients.png
  .. image:: images/obsinfo_plot_filter_coefficients_coeffs.png

ResponseList
-------------------------

:ref:`Template <response_list_filter_template>`

.. image:: images/obsinfo_plot_filter_response_list.png

See the instability possible if you specify a response list!

ADCONVERSION
-------------------------

:ref:`Template <ad_conversion_filter_template>`

.. image:: images/obsinfo_plot_filter_AD_Conversion.png
.. image:: images/obsinfo_plot_filter_AD_Conversion_coeffs.png
  
AD_Conversion filters have no frequency response.
They are implemented as ``Coefficients``objects with only one coefficient,
so you get the coefficients plot as well.

ANALOG
-------------------------

:ref:`Template <analog_filter_template>`

.. image:: images/obsinfo_plot_filter_analog.png

Analog filters are implemented as pole-zero objects with no poles and no zeros.

DIGITAL
-------------------------

:ref:`Template <digital_filter_template>`

.. image:: images/obsinfo_plot_filter_digital.png
.. image:: images/obsinfo_plot_filter_digital_coeffs.png

Response is the same as the analog filter, but it also shows off its single
"coefficient" (1 at offset=0)
