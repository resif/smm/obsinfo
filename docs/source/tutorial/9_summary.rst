
.. _Tutorial9:

*********************************
Summary
*********************************

**obsinfo** strives for reuse and flexibility.  To avoid repeating yourself
and reduce typing errors:

#. Use ``$ref`` as much as possible.
   This allows reuse across files and different campaigns.
#. Start from template files, keeping only necessary fields and fields you need
#. Use ``notes`` for information that you want to keep, but
   not to put in the StationXML file.
#. Add ``extras`` for information that has no place in StationXML, but that
   you'd like to store there anyway.  ``extras`` can be structured any
   way you want: they will be saved as JSON-encoded strings in StationXML
   ``Comments``
#. Use the ``default`` channel in your ``instrumentation_base`` file to avoid repeating
   redundant information.
#. Use ``configurations`` definitions to specify variations on the base definition

==================
Conclusion
==================

This finishes our tutorial.
For more detailed information please review the :ref:`Schemas` files, which
define all of the possible elements in information files.
Have fun using *obsinfo*!

If you find any issues or have any questions please use the Issues
functionality of gitlab: https://www.gitlab.com/resif/smm/obsinfo/issues

