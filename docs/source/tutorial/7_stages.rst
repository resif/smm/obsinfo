.. _Tutorial7:

*********************************************************
Building a stage_base file
*********************************************************

Stages are the building blocks of instrument responses.  Each **instrument component**
of an instrumentation channel consists of at least on stage.  stages
are always specified as lists of stage objects, with the topmost object
being applied first.

**Note**: since stages are chained, the output units of a stage must match
the input units of the next stage.

Digital stages
=====================
In StationXML, each digital stage has an input and an output sample rate.
In reality, most digital stages have no fixed sampling rate, they just apply
their coefficients and decimation to the output from the stage above. 
In obsinfo, therefore, only the top-level digital stage (usually the
delta-sigma input) should have a specified frequency: the final output sampling
rate will depend on the decimations introduced by the subsequent digital
filter stages.  **obsinfo** checks that the declared sample rate of the instrument
matches the calculated sample rate of the list of stages.


Every ``stage`` has an associated filter.  **obsinfo** allows simplified filter
specifications for stages that have no filter, as we will see in the next
tutorial section.  

Template stage_base file
===========================

The template stage_base file uses a pole-zero filter:

.. code-block:: console

    > obsinfo template stage_base


.. literalinclude:: ../../../src/obsinfo/_templates/TEMPLATE.stage_base.yaml
    :language: yaml
    
It validates with ``obsinfo schema`` and prints with ``obsinfo print``:

.. code-block:: console

    TEMPLATE.stage_base.yaml: 
        Stage:
            name: Funky filter [config: 10x]
            description: Oddball offbeat filter
            input_units: V
            output_units: V
            gain: 10
            gain_frequency: 10
            filter: PolesZeros
                stage_id: #-1 ("Funky filter [config: 10x]")
                11 poles, 6 zeros
                transfer_function_type: LAPLACE (RADIANS/SECOND)
                normalization_frequency: 10
                normalization_factor: 4.546e+17
            calibration_dates: OIDates:
                - 2004-12-31T00:00:00
                - 2024-12-31T00:00:00
            input_units_description: Volts
            output_units_description: Volts
            input_sample_rate: 100
            decimation_factor: 2
            decimation_offset: 0
            delay: 0
            correction: None

            
``obsinfo plot`` reveals that this is probably a broadband seismometer stage,
with a flat gain between about 100Hz and 200 seconds:

.. image:: images/obsinfo_plot_stage_base.png
