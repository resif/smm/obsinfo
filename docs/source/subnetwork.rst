.. _Subnetwork_Files:

***********************************************
Subnetwork Files
***********************************************


Subnetwork files are as far as most users will delve into **obsinfo**.
They let you specify all of the standard and obs-specific elements of
OBS deployments, without going into details about the instrumentation itself.

Here is an typical subnetwork file

.. literalinclude:: ../../src/obsinfo/_examples/subnetwork_files/EXAMPLE_typical.subnetwork.yaml
    :language: yaml


Subsections of subnetwork files
===============================================

Three dashes
------------
.. literalinclude:: ../../src/obsinfo/_examples/subnetwork_files/EXAMPLE_typical.subnetwork.yaml
    :language: yaml
    :end-before: format_version

Specificies that this is a new "data set"

Provenance information
-----------------------

.. literalinclude:: ../../src/obsinfo/_examples/subnetwork_files/EXAMPLE_typical.subnetwork.yaml
    :language: yaml
    :start-at: format_version
    :end-before: subnetwork

``$ref`` allows you to import information from another file (see :ref:`json_references`)

Operators and the FDSN network
------------------------------

.. literalinclude:: ../../src/obsinfo/_examples/subnetwork_files/EXAMPLE_typical.subnetwork.yaml
    :language: yaml
    :start-at: subnetwork
    :end-before: stations

Stations
--------

This is the biggest and most detailed section.  Each station is specified
by it's station code: we focus below on the first station: 

.. literalinclude:: ../../src/obsinfo/_examples/subnetwork_files/EXAMPLE_typical.subnetwork.yaml
    :language: yaml
    :start-after: stations
    :end-before: LSVE


Many of the values correspond directly to StationXML fields.
Exceptions are the ``processing`` fields, which are used to specify the
clock drift (and any leapseconds), and the ``datalogger_configuration``
field.

specifying the clock drift
~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``processing:clock_correction:drift`` element allows you to specify the instrument
and reference clock times at different moments, and how to interpolate
times in between the values.  Each row of the ``syncs_instrument_reference``
element contains simultaneous values of the  ``instrument`` time (first element)
and the ``reference`` time (second element).
The first row should be at/before the first data, and the last row should be
at/after the last data.
Intermediate rows, if any, allow you to specify intermediate values,
in case the drift is not purely linear.  Interpolation
can be `piecewise_linear`, `cubic_spline`, or `polynomial`: for the latter,
the polynomial coefficents should be specified and the
`syncs_instrument_reference` are used to verify the resulting values.

configurations
~~~~~~~~~~~~~~~~~~~~~~~~~~
``configuration`` fields allow you to select one of several configurations
defined in the ``instrumentation_base`` file, in one of its components
(``datalogger_base``, ``preamplifier_base`` or
``sensor_base``, or in one of it's components' stages. 
Dataloggers can generally sample at several different rates, the ``datalogger_configuration``
specified above selects the configuration corresponding to a 125 samples per
second rate.
Some dataloggers have other options (filter type, gain...):
the configuration string should represent all of the options.

The ``configuration`` you specify should be defined in the file referenced by the
``*_base`` element.  You can print a list of the available configurations using the
``obsinfo configurations`` subcommand.

If you don't have an ``instrumentation_base`` file, you can simply write a
text string after ``base``.  In this case ``obsinfo xml`` will create a
StationXML file without channels, but at least you will have written
the metadata needed for future processing.
You should still specify any configurations used: just write out the most
descriptive text possible (sampling rate, gain, filters...), which will allow
you to select the right configuration code once it is defined in a future
``instrumentation_base`` file.

Configuration and modification
=================================

Fields with a ``base`` element can be configured
and modified using the ``configuration`` and
``modification`` fields.  ``configuration`` allows you to use a pre-defined
configuration specified in the base element, ``modification`` allows you to
specify modifications "on the fly".  Detailed information is available
at :ref:`advanced_base_configuration_modifications`.
Below is a brief overview.

Below is the instrumentation section for an instrumentation for which you want
to specify the serial numbers for each instrument component:

.. code-block:: yaml

    instrumentation:
        base: {$ref: "instrumentation_bases/SPOBS2_INSU.instrumentation_base.yaml"}
        serial_number: "03"
        datalogger_configuration: "250sps"
        channel_modifications:
            "*": {sensor: {serial_number: "10"}}
            "H": {equipment: {serial_number: "12"}}

This will set the serial number for all channel's sensors to "10", except for
the "H" channel, which will be set to "12"

Below is an instrumentation section that specifies custom sensors for each
channel:

.. code-block:: yaml

    instrumentation:
        base: {$ref: "instrumentation_bases/SPOBS2_INSU.instrumentation_base.yaml"}
        serial_number: "03"
        datalogger_configuration: "250sps"
        channel_modifications:
            "*": {replace_sensor: {base: {$ref: "sensor_bases/GURALP_CMG3T.sensor_base.yaml"},
                                   configuration: "T3669"}}
            "H": {replace_sensor: {base: {$ref: "sensor_bases/HiTech_HTI-04_ULF.sensor_base.yaml"},
                                   serial_number: "12345"}}

This will replace the default sensor to a Guralp CMG3T seismometer with
configuration "T3669", and the sensor on channel "H" to a HiTech HTI-04-ULF hydrophone
with serial number "12345".  The configuration, which should be specified in
the sensor_base file, can specify the instrument response as well as the serial
number.

Another common use of configurations is to specify the station position
uncertainty, as you can see in every ``locations`` element:

.. code-block:: yaml

    instrumentation:
        base: {$ref: "instrumentation_bases/SPOBS2_INSU.instrumentation_base.yaml"}
        serial_number: "03"
        datalogger_configuration: "250sps"
        channel_modifications:
            "*": {replace_sensor: {base: {$ref: "sensor_bases/GURALP_CMG3T.sensor_base.yaml"},
                                   configuration: "T3669"}}
            "H": {replace_sensor: {base: {$ref: "sensor_bases/HiTech_HTI-04_ULF.sensor_base.yaml"},
                                   serial_number: "12345"}}

This will replace the default sensor to a Guralp CMG3T seismometer with
configuration "T3669", and the sensor on channel "H" to a HiTech HTI-04-ULF hydrophone
with serial number "12345".  The configuration, which should be specified in
the sensor_base file, can specify the instrument response as well as the serial
number.

``location_base`` configurations:
---------------------------------

Another common use of configurations is to specify the station position
uncertainty, as you can see in each ``locations`` element, for example:

.. code-block:: yaml

    locations:
        "00":
            base: {$ref: 'location_bases/EXAMPLE.location_base.yaml'}
            configuration: "BUC_DROP"
            position: {lon.deg:  -32.24117 , lat.deg: 37.28107 , elev.m: -1995}

The configuration "BUC_DROP" is specified in the location_base file:

.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/location_bases/EXAMPLE.location_base.yaml
    :language: yaml

The ``configurations`` element defines the configurations that can
be specified in a higher-level file.  In this example, the `BUC_DROP`
configuration specifies that the x, y and z uncertainties are 20 meters and
it puts text in each position's ``measurement_method`` field.  If you wanted to
specify that you deployed by BUC DROP but you have a more precise measurement
of each uncertainty, you could write: 

.. code-block:: yaml

    locations:
        "00":
            base: {$ref: 'location_bases/EXAMPLE.location_base.yaml'}
            configuration: "BUC_DROP"
            position: {lon.deg:  -32.24117 , lat.deg: 37.28107 , elev.m: -1995}
            modifications:
                uncertainties.m: {lon: 10.5, lat: 22.1, elev: 5.6}

or if you wanted to specify both the uncertainty and the measurement method,
you could write:

.. code-block:: yaml

    locations:
        "00":
            base: {$ref: 'location_bases/EXAMPLE.location_base.yaml'}
            position: {lon.deg:  -32.24117 , lat.deg: 37.28107 , elev.m: -1995}
            modifications:
                uncertainties.m: {lon: 10.5, lat: 22.1, elev: 5.6}
                measurement_method: "Pifometer"
                geology: "sediments"

(we threw in `geology`` for fun: you can enter anything that is specified in
the ``locations_base`` file)
            
It doesn't matter if ``modifications`` is written above or below
``configuration``: ``modifications`` are always applied after the ``configuration``.

``instrumentation_base`` example:
---------------------------------
            
The third station specifies an instrumentation ``configuration`` rather than
a ``serial_number``:

.. literalinclude:: ../../src/obsinfo/_examples/subnetwork_files/EXAMPLE_typical.subnetwork.yaml
    :language: yaml
    :start-after: LSVS

This allows you to pre-specify, for example, different responses
for different instrumentations.
The ``serial_number`` may also be specified in the configuration, saving you
having to write it in the ``subnetwork`` file.
If you also specify ``serial_number`` in the subnetwork file, it will overwrite
any serial_number value provided in the configuration.


Files read by the subnetwork file
===============================================

Below are the elements from each file referenced by the subnetwork file, except
for the :ref:`instrumentation_base_template` files,
which is described in the :ref:`Instrumentation_Files` page.

``persons/EXAMPLE.person.yaml``
--------------------------------------------------------------

.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/persons/EXAMPLE.person.yaml
    :language: yaml
    :start-after: person


``networks/EXAMPLE.network.yaml``
--------------------------------------------------------------

.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/networks/EXAMPLE.network.yaml
    :language: yaml
    :start-after: network


``location_bases/EXAMPLE.location_base.yaml``
--------------------------------------------------------------

.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/location_bases/EXAMPLE.location_base.yaml
    :language: yaml
    :start-after: location_base

``timing_bases/EXAMPLE.timing_base.yaml``
--------------------------------------------------------------

.. literalinclude:: ../../src/obsinfo/_examples/instrumentation_files/timing_bases/EXAMPLE.timing_base.yaml
    :language: yaml
    :start-after: timing_base