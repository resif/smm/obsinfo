===========
CHANGELOG
===========

-------------
Version 1.0
-------------

Information files
***************************************************

In all files
----------------------------------
- ``{$ref: "{path}/{filename}.{filetype}.json#{inner_path}"}`` can be written as
  ``{$ref: "{path}/{filename}.{filetype}.json"}`` if ``{inner_path}`` == ``{filetype}``
  
In ``subnetwork`` files
----------------------------------

- ``instrumentation:base`` can be a simple string instead of a reference
  to an ``instrumentation_base.json`` file.  In this case, ``obsinfo xml`` will
  generate a station-level file (no channel or reponse information).

- ``position`` elements are now specified by ``lat.deg``, ``lon.deg`` and ``elev.m``
  (was ``lat``, ``lon`` and ``elev``)

  - ``uncertainty.m`` elements are still ``lat``, ``lon`` and ``elev`` (because
    ``uncertainty.m`` specifies the units (meters))

- The ``station:location_code`` field is no longer required if only one location
  is provided, in which case the station will use that location's code.
- Empty location codes are allowed (but not recommended), for compatibility
  with StationXML
- ``comments``: added optional sub-elements ``id`` and ``subject``, to conform
  to StationXML
- ``station:site`` is no longer required.  If it is not provided, the StationXML
  ``site`` field will be set to the station code.
- clock correction information has been significantly altered to allow for
  multiple synchronizations, polynomial fits and cubic spline fits.
- leapsecond(s) are now specified at the top level of the ``subnetwork`` file.
  Applied leapsecond corrections can still be specified for each station
- ``network:name`` was removed (there is no equivalent in StationXML)

In other specific files
----------------------------------

- ``orientation`` specification has changed:

  - Was: ``orientation: {{code_value}: {azimuth.deg: , dip.deg: }}``
  - Is: ``orientation: {code: {value}, azimuth.deg: {value}, dip.deg: {value}}``
  
- The replacement code string is changed from ``^`` to ``replace_``
- ``seed_codes:band_base` is replaced by ``seed_codes:band``, possible values are:

  - ``shortperiod``: band code will be the appropriate '<=10s' code for the sample rate
  - ``broadband``: band code will be the appropriate '> 10s' code for the sample rate
  - ``A``, ``I``, ``O``, ``L``, or ``S``: the band code will be as specified,
    regardless of the sample rate (allows historical codes such as 'LOG'
    and 'SOH')

- The ``ADCONVERSION`` filter now has A/D converter-specific fields
      
Processing
***************************************************

- The ``ADCONVERSION`` filter calculates it's own gain.
- If a filter calculates its own gain (only ``ADCONVERSION`` for now), **obsinfo**
  will compare this value to the stage-specified gain and output an error message
  if they differ by more than 1%
      
commmand-line scripts
***************************************************

Subcommands have been added to the obsinfo command-line scripts.  We simplified
the subcommand names where possible.  If you
type ``obsinfo -h`` you will see a brief description of each subcommand

Renamed command-line scripts
----------------------------

==========================   ==================
Old Name                     New name
==========================   ==================
``obsinfo-setup``            ``obsinfo setup``
``obsinfo-print_version``    ``obsinfo version``
``obsinfo-validate``         ``obsinfo schema``
``obsinfo-print``            ``obsinfo print``
``obsinfo-makeStationXML``   ``obsinfo xml``
==========================   ==================

``obsinfo xml`` now validates the output file using FDSN's
``stationxml_validator``

New subcommands
------------------------
``plot``, ``configurations``, ``template``


Unchanged
------------------------

The ``obsinfo-makescripts-XXX`` scripts have not yet been renamed, as they
are add-ons to the base *obsinfo* system

v1.0 subversions
*****************

- v1.0b1: first pip-released version 1
- v1.0b2: fixed path bug in `obsinfo setup`, missing `stationxml-validator`
- v1.0b3: fixed  bug in which subnetwork operators were not transmitted down
  to station level
      
-------------
Version 0.111
-------------

Renamed elements
***************************************************

- ``network`` to ``subnetwork``
- ``network:network_info`` to ``subnetwork:network``
- ``location_base:localisation_method`` to ``location_base:measurement_method``

Improved StationXML conformity
***************************************************

- ``authors`` renamed to ``persons``, now has same fields as StationXML Person
- ``operator`` subelements are now ``agency`` (string), ``contacts``
  (list of persons), ``website`` (URL string)
- changed several elements corresponding to StationXML multi-instance
  elements into arrays:

  - ``operators``, ``operator:contacts``, ``person:names``, ``person:agencies``,
    ``person:emails``, ``person:phones``
  - Note: did NOT change ``equipment`` to ``equipments``, because this 
    would complicate updating equipment using ``modifications`` and
    ``configuration``

- added ``network:operator`` element
- removed ``instrumentation:operator`` element
- Added StationXML-compatible id/source elements

  - stage, filter and equipment: ``resource_id``
  - channel and station: ``source_id``, ``external_references`` and ``identifiers``
  - network: ``source_id``, ``identifiers``

- Added other StationXML fields:

  - channel, station and network: ``restricted_status``
  - location and station: ``water_level.m``
  - stage: ``calibration_dates``
  - equipment: ``installation_date``, ``removal_date``, ``calibration_date``

Improved AROL conformity
***************************************************

- Changed ``poles`` and ``zeros`` specifications from 2-element arrays to
  string ``x + yj``
- Replaced ``offset`` by ``delay.samples``

Added base-configuration-modifications structure
***************************************************

To the following elements:

- instrumentation: ``instrumentation``, ``datalogger``, ``preamplifier``,
  ``sensor`` and ``stage``
- other: ``location`` and ``clock_correction_linear``

Other cleanup
***************************************************

- replaced ``network: reference_name`` and ``network: campaign_reference_name`` by
  ``subtnework: reference_names: {campaign: , operator}``
- changed ``azimuth`` and ``dip`` into objects with ``value``, ``uncertainty``
  and ``measurement_method`` fields (only ``value`` is required)
- added ``delay.seconds`` for analog filters (``ANALOG`` and ``PolesZeros``)
- moded ``serial_numner``, ``modifications`` and ``channel_modifications`` 
  from ``station`` level to  ``station: instrumentation`` level

-------------
Version 0.110
-------------

- 0.110.11:
  - Added obsinfo-makescripts-LC2SDS

- 0.110.12:
  - Fixed bug in clock correction reading (leapsecond and drift)
  - Made obsinfo-makescripts-LC2SDS write to campaign directory by default
  - Fixed some (not all) StationXML bugs:

    - Made clock correction Comments using JSON
    - Removed `restrictedStatus='unknown'` (invalid) from StationXML
    - Fixed bugs in Decimation filters and stage #s
    - Standardized uncertainties in Poles, Zeros, Elevation, Dip, Azimuths

  - Bugs that will probably have to wait for version 0.111:
  
    - instrumentation serial number and configurations not accounted for

- 0.110.13:

  - Made all obsinfo-test cases work
  - Added "serial_number" to station level (temporary fix before v 0.111)
  - Updated JSON schema to draft07, allowing more precise/compact information
    on mutiple-choice errors (e.g., different types of filter)

- 0.110.14:

  - Added StationXML test case and made it work
  - made ``python -m unittest discover`` work

- 0.110.15:

  - corrected JSON validation schema for ``orientation_code``
  - improved azimuth.deg and dip.deg schema definitions
  - Fixed a bug in reporting schema validation errors (in ``obsmetadata.py``)
    introduced when shifting to draft07
  - Reduced a bug when a jsonref points to an inexistent pointer within
    a file
  - Uses Python 3.8-dependent syntax despite only requiring Python 3!

- 0.110.16:

  - Clean up information file validation, removing many redundancies in
    main/validate.py and hopefully fixing bug where schema files lacking
    ".schema.yaml" are searched for
  - Requires Python 3.7 syntax and only uses 3.7-dependent syntax
  - *post2*: streamlines ``obsinfo-makescripts_LC2SDS``'s output

- 0.110.17:

  - Improvements to channel_modifications:

    - Make reading a new sensor, datalogger or preamplifier work
    - Enable shortcuts for entering serial number
    - Updated documentation

  - Added developer and information_file documentation
    in channel_modifications/
  - Moved tests up to united top-level directory
  - `offset` can be a non-integer

- 0.110.18:

  - Added datacite information file
  - Added schema and validation of datacite, author, location_base,
    network_info and operator information files
  - Cleaned up some information file validation glitches
    
- 0.111: Major upgrade for structure and StationXML compatibility

  - Language changes

    - renamed:

      - ``network`` to ``subnetwork``
      - ``network:network-info`` to ``subnetwork:network``
      - ``response_stages`` to ``stages``
      - `azimuth` and `dip` to `azimuth.deg` and `dip.deg`
      - `stations_operator` to `subnetwork:operators`

    - `subnetwork:operators` is now a list, in accordance with StationXML.
      This element is the default operator for each station
    - Added `subnetwork:network:operators` element
    - Changed `azimuth.deg` and `dip.deg` from lists (`[value, uncert]`) to uncertainty dicts
      (`{'value': number, 'uncert': number, 'measurement_method': str}`)
    - Require `stages` in `datalogger`, `preamplifier` and `sensor`
    - Created a general base-configuration-modification methodology (see
      obsinfo.readthedocs.org/Advanced) and applied
      it to `instrumentation`, `datalogger`, `sensor`, `preamplifier`, `stage`,
      `location` and `timing` elements.
    - Added optional `resourceId` sub-element to `subnetwork`, `station`, `channel`,
      `equipment` and `filter` elements
    - Added optional `custom_fields` sub-element to `equipment` and `filter` elements
    - Added `measurement_method` to `location_base` element
    - Standardized structure and nomenclature of elements that can be declared
      multiple times in StationXML:

      - In obsinfo, becomes a list and has 's' and end of name:

        - ``operator`` -> ``operators``, ``contact`` -> ``contacts``

    - renamed ``author`` type to ``person``, made ``person`` match StationXML definition

      - ``person`` sub-elements are ``names`` (list of str), ``agencies`` (list of str),
        ``emails`` (list of email-format str) and ``phones`` (list of phone #s)

    - removed 'instrumentation_operator' (was unused in v0.110)
    - Brought instrumentation modifiers within "instrumentation" object

      - Now has ``base``, ``serial_number``, ``configuration``, ``modifications``
        and ``channel_modifications`` subelements

    - Made 'operator' conform to StationXML standards

      - subelements are now ``agency``, ``contacts`` and ``website``
      - ``contacts`` is a list of ``person`` s

    - created optional ``reference_names`` element

      - contains ``campaign`` (old ``campaign_ref_name``) and ``operator`` (old
        ``operator:ref_name``)

    - un-required ``channels`` within ``instrument_base``
    - added optional ``subnetwork:stations:{NAME}:operator``, overrides
      ``stations_operator`` for this station.
    - changed required ``offset`` to ``delay.samples`` (digital filters) or
      ``delay.seconds`` (analog filters). 
      ``offset`` was not the appropriate field for specifying
      the filter delay, it still exists but is limited to 0-1, usually 0
    - changed non-standard filter names to ALL CAPS: ``ADConversion``->``AD_CONVERSION``,
      ``Analog``->``ANALOG``, ``Digital``->``DIGITAL``
    - complex numbers are now specified as text (``[10, -4]`` becomes ``'10-4j'``)

  - Bugs fixed, including:

      - serial number is now correctly entered for ``station:equipment``

- v0.111.post6:

  - Made v0.111 part of CHANGELOG.rst
  - Changes needed for StationXMLs to validate using stationxml_validator

    - `Decimation:Offset` (required element) added
