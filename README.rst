README
===================

*obsinfo* is a system for for creating FDSN-standard data and metadata for
ocean bottom seismometers using standardized, easy-to-read information files 


`Documentation <https://obsinfo.readthedocs.io>`_

`Source code <https://gitlab.com/resif/smm/obsinfo>`_
