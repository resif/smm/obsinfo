Most of these schemas correspond to valid file types.  The exceptions are:

- definitions: basic entities used in many other entities
- iris_units: used in stage_base, but defined externally for clarity
- stages: used in datalogger_base, preamplifier_base and sensor_base