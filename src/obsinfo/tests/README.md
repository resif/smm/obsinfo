More involved tests, take longer than the suite at the base level

This isn't a unittest suite, but an evaluation of the _example/
and _templates/ databases.

## test_* files

- `test_datapath.py`: ???
- `test_examples.py`: validate, print, configurations for all `_examples/` files
- `test_templates.py`: validate, print, plot for all `_templates/` files
- `test_xml.py`: create two StationXML files and compare with references
- `test_remoteGithub.py`:  Tests github read.  Not sure that this actually
  tests anything
- `run_test_script.py`: ???.  Not run by pytest

## Stages of work for v1.0

- Renamed test.py to `run_test_script.py` so that it can still be called
  by `obsinfo-test` and `obsinfo-print`, but not discovered by
  `python -m unittest`
  
- Renamed `test_main.py` to `test_xml.py`: it tests creation of StationXML
  
- Split test.py into unittest discoverable units:
    - `test_jsonref.py`: json ref tests, relatively fast, moved up to
      top-level `tests/`
    - `test_datapath.py`: should only test an information file database.
      I want to change this so it can EITHER test the datapath or the
      _examples/, and so that it can be called by run_test_script.py.
      
    - `test_infofile.py`: should only contain specific tests of infofile
      reading and evaluation, using in-code "info-files" as well as ones in the
      data/ directory.
        - I 
        
- Removed test_infofile.py, which relied on information files that would
  have to be changed at each language upgrade (there are already enough in
  _examples/ and _templates/)

- test_datapath.py:
    - removed test_test_filters() and test_PZ_conditionals()

## Added `test_examples.py` and `test_templates.py`

Runs obsinfo's `schema`, `print`, `plot` and `configuration` subcommands.

Allows a full test of the example/ and template/ databases.

## ToDo

- Figure out what `run_test_script.py` does
- Figure out what `test_datapath.py` does
- Figure out how to make `test_remoteGithub.py` useful

