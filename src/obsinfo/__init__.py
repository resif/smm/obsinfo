name = "obsinfo"
"""
command-line programs for creating StationXML files and time-corrected data

commmand-line programs:
    obsinfo: the main command, type `obsinfo -h` for subcommands
    obsinfo-make...: addon commands for specific systems
"""
